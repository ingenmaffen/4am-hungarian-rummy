const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: "./app.module.ts",
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
    ],
  },
  resolve: {
    extensions: [".ts"],
  },
  output: {
    filename: "app.js",
    path: path.resolve(__dirname, "public"),
  },
  context: path.join(__dirname, "src/client/"),
  plugins: [
    new CopyWebpackPlugin({ patterns: [{ from: "assets", to: "assets" }] }),
  ],
};
