# 4am Hungarian Rummy

This is a project that I started in 2017 (in Unity), lost all the source code and restarted it as a web application. Later I chose this project to be my thesis project at the university.

The application is an implementation of the game Rummy, but with the hungarian rule set.

## Demo

The application can be accessed at [https://rummy-4am.herokuapp.com](https://rummy-4am.herokuapp.com)

## Development

To start the app, install the dependencies and start it:

### Setup

```
npm install
```

### Run Application

```
npm start
```

### Run app in development mode

You can start the server in development mode with

```
npm serverWatch
```

which restart itself everytime any change is detected in the app.

To run the frontend code in development mode, run

```
npm tsWatch
```

### Notes

If you find any bug, let me know by sending an e-mail to [ingenmaffen@gmail.com](mailto:ingenmaffen@gmail.com).

The roadmap for the application can be found on [Trello](https://trello.com/b/F2ENVYbF/4am-hungarian-rummy).
