import { AppComponent } from "./app/app.component";
import "./styles/app.scss";

export module AppModule {
  const mainComponent: AppComponent = new AppComponent();
}
