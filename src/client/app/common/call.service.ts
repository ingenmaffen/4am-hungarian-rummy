import { Texture, TextureLoader } from "three";

export class CallService {
  private textureLoader = new TextureLoader();

  public getTexture(url): Texture {
    return this.textureLoader.load(url);
  }

  public getSocket(): any {
    return window["io"]();
  }
}
