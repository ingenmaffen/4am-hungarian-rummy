import { CardConst } from "./interfaces";

// commonly used variables
const variables = {
  cardOrder: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "B", "D", "K", "A"],
  table: {
    position: {
      x: -2.5,
      y: -2.5,
      z: -5.5,
    },
    size: {
      radius: 5,
      height: 0.33,
    },
  },
  cardSize: {
    width: 1,
    height: 1.5,
    depth: 0.0001,
  },
  playerMostRightCard: {
    position: {
      x: 1.5,
      y: -1.25,
      z: -1.5,
    },
    rotation: {
      x: -Math.PI / 6,
      y: 0,
      z: -Math.PI / 6,
    },
  },
  opponentMostRightCard: {
    position: {
      x: -4.5,
      y: -0.5,
      z: -8.5,
    },
    rotation: {
      x: 0,
      y: (7 * Math.PI) / 6,
      z: Math.PI / 6,
    },
  },
};

export const environment = {
  ...variables,
  deckBottomCard: {
    position: {
      x: variables.table.position.x - (variables.cardSize.width / 2 + 0.25),
      y: variables.table.position.y + (variables.table.size.height / 2 + 0.05),
      z: variables.table.position.z,
    },
    rotation: {
      x: Math.PI / 2,
      y: 0,
      z: 0,
    },
  },
  thrownBottomCard: {
    position: {
      x: variables.table.position.x + (variables.cardSize.width / 2 + 0.25),
      y: variables.table.position.y + (variables.table.size.height / 2 + 0.05),
      z: -5.5,
    },
    rotation: {
      x: -Math.PI / 2,
      y: 0,
      z: 0,
    },
  },
  playerGroupsMostLeftCard: [
    {
      position: {
        x: variables.table.position.x,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: -1.5,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
    {
      position: {
        x: variables.table.position.x,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: -3.5,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
    {
      position: {
        x: variables.table.position.x - 3,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: -3.5,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
    {
      position: {
        x: variables.table.position.x + 2.5,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: -2.5,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
  ],
  opponentGroupsMostRightCard: [
    {
      position: {
        x: variables.table.position.x + 2.5,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: variables.table.position.z - 0.5,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
    {
      position: {
        x: variables.table.position.x - 0.5,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: variables.table.position.z - 2,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
    {
      position: {
        x: variables.table.position.x + 1.5,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: variables.table.position.z - 3,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
    {
      position: {
        x: variables.table.position.x - 1.5,
        y:
          variables.table.position.y + (variables.table.size.height / 2 + 0.05),
        z: variables.table.position.z - 3.5,
      },
      rotation: {
        x: Math.PI / 2,
        y: 0,
        z: 0,
      },
    },
  ],
};

export const cards: CardConst[] = [
  {
    id: 0,
    value: 2,
    order: 0,
    suit: "clubs",
    texture: "clubs2",
  },
  {
    id: 1,
    value: 3,
    order: 1,
    suit: "clubs",
    texture: "clubs3",
  },
  {
    id: 2,
    value: 4,
    order: 2,
    suit: "clubs",
    texture: "clubs4",
  },
  {
    id: 3,
    value: 5,
    order: 3,
    suit: "clubs",
    texture: "clubs5",
  },
  {
    id: 4,
    value: 6,
    order: 4,
    suit: "clubs",
    texture: "clubs6",
  },
  {
    id: 5,
    value: 7,
    order: 5,
    suit: "clubs",
    texture: "clubs7",
  },
  {
    id: 6,
    value: 8,
    order: 6,
    suit: "clubs",
    texture: "clubs8",
  },
  {
    id: 7,
    value: 9,
    order: 7,
    suit: "clubs",
    texture: "clubs9",
  },
  {
    id: 8,
    value: 10,
    order: 8,
    suit: "clubs",
    texture: "clubs10",
  },
  {
    id: 9,
    value: 10,
    order: 9,
    suit: "clubs",
    texture: "clubsB",
  },
  {
    id: 10,
    value: 10,
    order: 10,
    suit: "clubs",
    texture: "clubsD",
  },
  {
    id: 11,
    value: 10,
    order: 11,
    suit: "clubs",
    texture: "clubsK",
  },
  {
    id: 12,
    value: 10,
    order: 12,
    suit: "clubs",
    texture: "clubsA",
  },
  {
    id: 13,
    value: 2,
    order: 0,
    suit: "spades",
    texture: "spades2",
  },
  {
    id: 14,
    value: 3,
    order: 1,
    suit: "spades",
    texture: "spades3",
  },
  {
    id: 15,
    value: 4,
    order: 2,
    suit: "spades",
    texture: "spades4",
  },
  {
    id: 16,
    value: 5,
    order: 3,
    suit: "spades",
    texture: "spades5",
  },
  {
    id: 17,
    value: 6,
    order: 4,
    suit: "spades",
    texture: "spades6",
  },
  {
    id: 18,
    value: 7,
    order: 5,
    suit: "spades",
    texture: "spades7",
  },
  {
    id: 19,
    value: 8,
    order: 6,
    suit: "spades",
    texture: "spades8",
  },
  {
    id: 20,
    value: 9,
    order: 7,
    suit: "spades",
    texture: "spades9",
  },
  {
    id: 21,
    value: 10,
    order: 8,
    suit: "spades",
    texture: "spades10",
  },
  {
    id: 22,
    value: 10,
    order: 9,
    suit: "spades",
    texture: "spadesB",
  },
  {
    id: 23,
    value: 10,
    order: 10,
    suit: "spades",
    texture: "spadesD",
  },
  {
    id: 24,
    value: 10,
    order: 11,
    suit: "spades",
    texture: "spadesK",
  },
  {
    id: 25,
    value: 10,
    order: 12,
    suit: "spades",
    texture: "spadesA",
  },
  {
    id: 26,
    value: 2,
    order: 0,
    suit: "diamonds",
    texture: "diamonds2",
  },
  {
    id: 27,
    value: 3,
    order: 1,
    suit: "diamonds",
    texture: "diamonds3",
  },
  {
    id: 28,
    value: 4,
    order: 2,
    suit: "diamonds",
    texture: "diamonds4",
  },
  {
    id: 29,
    value: 5,
    order: 3,
    suit: "diamonds",
    texture: "diamonds5",
  },
  {
    id: 30,
    value: 6,
    order: 4,
    suit: "diamonds",
    texture: "diamonds6",
  },
  {
    id: 31,
    value: 7,
    order: 5,
    suit: "diamonds",
    texture: "diamonds7",
  },
  {
    id: 32,
    value: 8,
    order: 6,
    suit: "diamonds",
    texture: "diamonds8",
  },
  {
    id: 33,
    value: 9,
    order: 7,
    suit: "diamonds",
    texture: "diamonds9",
  },
  {
    id: 34,
    value: 10,
    order: 8,
    suit: "diamonds",
    texture: "diamonds10",
  },
  {
    id: 35,
    value: 10,
    order: 9,
    suit: "diamonds",
    texture: "diamondsB",
  },
  {
    id: 36,
    value: 10,
    order: 10,
    suit: "diamonds",
    texture: "diamondsD",
  },
  {
    id: 37,
    value: 10,
    order: 11,
    suit: "diamonds",
    texture: "diamondsK",
  },
  {
    id: 38,
    value: 10,
    order: 12,
    suit: "diamonds",
    texture: "diamondsA",
  },
  {
    id: 39,
    value: 2,
    order: 0,
    suit: "hearts",
    texture: "hearts2",
  },
  {
    id: 40,
    value: 3,
    order: 1,
    suit: "hearts",
    texture: "hearts3",
  },
  {
    id: 41,
    value: 4,
    order: 2,
    suit: "hearts",
    texture: "hearts4",
  },
  {
    id: 42,
    value: 5,
    order: 3,
    suit: "hearts",
    texture: "hearts5",
  },
  {
    id: 43,
    value: 6,
    order: 4,
    suit: "hearts",
    texture: "hearts6",
  },
  {
    id: 44,
    value: 7,
    order: 5,
    suit: "hearts",
    texture: "hearts7",
  },
  {
    id: 45,
    value: 8,
    order: 6,
    suit: "hearts",
    texture: "hearts8",
  },
  {
    id: 46,
    value: 9,
    order: 7,
    suit: "hearts",
    texture: "hearts9",
  },
  {
    id: 47,
    value: 10,
    order: 8,
    suit: "hearts",
    texture: "hearts10",
  },
  {
    id: 48,
    value: 10,
    order: 9,
    suit: "hearts",
    texture: "heartsB",
  },
  {
    id: 49,
    value: 10,
    order: 10,
    suit: "hearts",
    texture: "heartsD",
  },
  {
    id: 50,
    value: 10,
    order: 11,
    suit: "hearts",
    texture: "heartsK",
  },
  {
    id: 51,
    value: 10,
    order: 12,
    suit: "hearts",
    texture: "heartsA",
  },
  {
    id: 52,
    value: -1,
    order: -1,
    suit: "joker",
    texture: "joker1",
  },
  {
    id: 53,
    value: -1,
    order: -1,
    suit: "joker",
    texture: "joker2",
  },
  {
    id: 54,
    value: -1,
    order: -1,
    suit: "joker",
    texture: "joker3",
  },
];
