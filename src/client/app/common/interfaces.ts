import { Mesh } from "three";

export interface Card {
  id: number | null; // 0-54
  value: number | null; // 2-10
  order: number | null; // 0-13
  suit: Suit;
  selected: boolean;
  cardMesh: Mesh;
  joker?: boolean;
}

export interface CardPosition {
  position: Dimension;
  rotation: Dimension;
}

export interface CardConst {
  id: number | null; // 0-54
  value: number | null; // 2-10
  order: number | null; // 0-13
  suit: Suit;
  texture: string;
}

export interface Dimension {
  x: number;
  y: number;
  z: number;
}

export interface TableCards {
  player: TableCardsPerPlayer;
  opponent: TableCardsPerPlayer;
}

export interface TableCardsPerPlayer {
  open: boolean;
  groups: Array<Card[]>;
}

export interface TurnState {
  current: CurrentTurnState;
}

export interface MenuItem {
  text: string;
  callback: Function;
}

export interface Options {
  music: number;
  sfx: number;
}

type CurrentTurnState = null | "startTurn" | "inTurn" | "endTurn";

type Suit = null | "diamonds" | "spades" | "hearts" | "clubs" | "joker";
