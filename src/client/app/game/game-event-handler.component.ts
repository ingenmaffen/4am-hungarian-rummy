import { TurnState } from "../common/interfaces";
import { GameObjectComponent } from "./game-objects.component";
import { SoundService } from "./sound.service";

import * as $ from "jquery";

export class GameEventHandlerComponent {
  constructor(
    private socket: any,
    private turnState: TurnState,
    private gameObjectComponent: GameObjectComponent,
    private soundService: SoundService
  ) {
    // play sound on request
    this.socket.on("getRequestFrom", (_data) =>
      this.soundService.requestSound.play()
    );

    this.socket.on("initialCards", (cardIds: number[]) => {
      // player is player one
      if (cardIds.length === 15) {
        this.turnState.current = "inTurn";
        cardIds.forEach((cardId) => {
          this.gameObjectComponent.fromDeckToHand(cardId);
        });
        for (let i = 0; i < 14; i++) {
          this.gameObjectComponent.fromDeckToOpponent();
        }
        // TODO: translation
        $("#turn-state").text("Your turn");
      } else {
        this.turnState.current = "endTurn";
        for (let i = 0; i < 15; i++) {
          this.gameObjectComponent.fromDeckToOpponent();
        }

        cardIds.forEach((cardId) => {
          this.gameObjectComponent.fromDeckToHand(cardId);
        });
        // TODO: translation
        $("#turn-state").text("Opponent's turn");
      }
    });

    this.socket.on("opponentThrow", ({ index, cardId }) => {
      this.gameObjectComponent.opponentThrow(index, cardId);
      // TODO: translation
      $("#turn-state").text("Your turn");
    });

    this.socket.on("opponentPickUpThrown", () => {
      this.gameObjectComponent.formThrownToOpponent();
    });

    this.socket.on("opponentPickUpFromDeck", () => {
      this.gameObjectComponent.fromDeckToOpponent();
    });

    this.socket.on("opponentPutDownGroup", (indexes: number[]) => {
      this.gameObjectComponent.opponentPutDownCards(indexes);
    });

    this.socket.on("opponentPutDownGroupOpen", ({ indexes, group }) => {
      this.gameObjectComponent.opponentPutDownCards(indexes, group);
    });

    this.socket.on("opponentPickUpGroup", (groupLength: number) => {
      this.gameObjectComponent.opponentPickUpCardsFormTable(groupLength);
    });

    this.socket.on("opponentAddCardToGroupOpen", ({ group, index }) => {
      this.gameObjectComponent.opponentAddCardToGroupOpen(group, index);
    });

    this.socket.on("opponentAddCardToGroupClosed", ({ groupLength, index }) => {
      this.gameObjectComponent.opponentAddCardToGroupClosed(groupLength, index);
    });

    this.socket.on(
      "opponentOpensCards",
      (opponentTableCards: Array<number[]>) => {
        this.gameObjectComponent.opponentOpenCards(opponentTableCards);
      }
    );

    this.socket.on("opponentReplaceJoker", ({ group, cardIndexes, cards }) => {
      this.gameObjectComponent.opponentReplaceJoker(group, cardIndexes, cards);
    });

    this.socket.on("reshuffleThrown", () => {
      this.gameObjectComponent.reshuffleThrown();
    });

    this.socket.on("win", () => {
      this.gameObjectComponent.playerWon();
    });

    this.socket.on("lose", () => {
      this.gameObjectComponent.playerLost();
    });

    this.socket.on("opponentLeft", () => {
      this.gameObjectComponent.opponentLeft();
    });
  }
}
