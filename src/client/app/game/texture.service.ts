import { CallService } from "../common/call.service";
import {
  LinearFilter,
  MeshBasicMaterial,
  RepeatWrapping,
  Texture,
} from "three";
import { cards } from "../common/environment";

export class TextureService {
  private callService = new CallService();
  public textures: any = {
    background: null,
    table: null,
    cards: {},
  };

  constructor() {
    this.loadTextures();
  }

  private loadTextures() {
    const urlBase = "static/assets/images";
    // TODO: enable low resolution textures to be used (options)
    this.textures.background = this.callService.getTexture(
      `${urlBase}/game-background-4x.jpg`
    );

    this.textures.table = this.callService.getTexture(
      "static/assets/images/table-texture.jpg"
    );

    // textures for card back and side
    const backTexture = this.callService.getTexture(
      `${urlBase}/cards/back.jpg`
    );
    const whiteTexture = this.callService.getTexture(
      `${urlBase}/cards/white.jpg`
    );

    // empty card texture
    const blankCardTexture = [
      new MeshBasicMaterial({ map: whiteTexture }),
      new MeshBasicMaterial({ map: whiteTexture }),
      new MeshBasicMaterial({ map: whiteTexture }),
      new MeshBasicMaterial({ map: whiteTexture }),
      new MeshBasicMaterial({ map: whiteTexture }), //face
      new MeshBasicMaterial({ map: backTexture }), //back
    ];
    this.textures.cards.blank = blankCardTexture;

    cards.forEach((cardObject) => {
      const texture = this.callService.getTexture(
        `${urlBase}/cards/${cardObject.texture}.jpg`
      );
      const textureObject = [
        new MeshBasicMaterial({ map: whiteTexture }),
        new MeshBasicMaterial({ map: whiteTexture }),
        new MeshBasicMaterial({ map: whiteTexture }),
        new MeshBasicMaterial({ map: whiteTexture }),
        new MeshBasicMaterial({ map: texture }), //face
        new MeshBasicMaterial({ map: backTexture }), //back
      ];
      this.textures.cards[cardObject.texture] = textureObject;
    });

    this.mirrorBackgroundTexture();

    // TODO: can be turned on/off
    this.setTextureFilterToLinear(this.textures.background);
    this.setTextureFilterToLinear(this.textures.table);
  }

  /*
   * three.js resizes textures by default
   * because the minFilter is set to THREE.LinearMipmapLinearFilter
   * source: https://threejs.org/docs/#api/en/textures/Texture.minFilter
   *
   * By using LinearFilter, the background and table textures are much cleaner
   * but this should be a performance option later
   */
  private setTextureFilterToLinear(texture: Texture) {
    texture.minFilter = LinearFilter;
  }

  private mirrorBackgroundTexture() {
    this.textures.background.wrapS = RepeatWrapping;
    this.textures.background.repeat.x = -1;
  }
}
