export class GameService {
  constructor(private socket: any, public roomId: string) {}

  public throwCard(cardId: number, callback: Function) {
    this.socket.emit("throwCard", { roomId: this.roomId, cardId });

    this.socket.once("canThrow", () => {
      callback();
    });
  }

  public pickUpFromThrown(callback: Function) {
    this.socket.emit("pickUpThrownCard", this.roomId);

    this.socket.once("canPickUpFromThrown", () => {
      callback();
    });
  }

  public pickUpFromDeck(callback: Function) {
    this.socket.emit("pickUpDeckCard", this.roomId);

    this.socket.once("canPickUpFromDeck", (cardId: number) => {
      callback(cardId);
    });
  }

  public putDownGroup(group: number[], callback: Function) {
    this.socket.emit("putDownGroup", { roomId: this.roomId, group });

    this.socket.once("canPutDownGroup", () => {
      callback();
    });
  }

  public pickUpGroup(group: number[], callback: Function) {
    this.socket.emit("pickUpGroup", { roomId: this.roomId, group });

    this.socket.once("canPickUpGroup", () => {
      callback();
    });
  }

  public addCardToGroup(group: number[], cardId: number, callback: Function) {
    this.socket.emit("addCardToGroup", { roomId: this.roomId, group, cardId });

    this.socket.once("canAddCardToGroup", () => {
      callback();
    });
  }

  public openCards(callback: Function) {
    this.socket.emit("openCards", this.roomId);

    this.socket.once("canOpenCards", () => {
      callback();
    });
  }

  public replaceJokerInGroup(
    group: number[],
    cards: number[],
    callback: Function
  ) {
    this.socket.emit("replaceJoker", {
      roomId: this.roomId,
      group,
      cards,
    });

    this.socket.once("canReplaceJoker", () => {
      callback();
    });
  }

  public returnToLobby() {
    this.socket.emit("returnToLobby", {
      roomId: this.roomId,
    });
  }
}
