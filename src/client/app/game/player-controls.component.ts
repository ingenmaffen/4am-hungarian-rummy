import { Mesh } from "three";
import { environment, cards } from "../common/environment";
import { Card, TableCards, TurnState } from "../common/interfaces";
import { GameActionsComponent } from "./actions.component";
import { AnimationsService } from "./animations.service";
import { GameService } from "./game.service";

import * as $ from "jquery";

export class PlayerControlsComponent {
  private gameActionHandler: GameActionsComponent = new GameActionsComponent();
  private cardsHidden: boolean = false;
  private cardValueOnTable: number = 0;

  constructor(
    private playerCards: Card[],
    private thrown: Card[],
    private tableCards: TableCards,
    private animationsService: AnimationsService,
    private service: GameService,
    private turnState: TurnState
  ) {}

  public handleClickOnCardInHand(object: Mesh) {
    const card = this.playerCards.find(
      (card) => card.cardMesh.geometry.uuid === object.geometry.uuid
    );
    const index = this.playerCards.indexOf(card);
    if (card) {
      if (!card.selected) {
        this.animationsService.animateCardMovement(
          card,
          {
            position: this.animationsService.getSelectedCardPosition(
              this.playerCards,
              index,
              object
            ),
            rotation: {
              x: card.cardMesh.rotation.x,
              y: card.cardMesh.rotation.y,
              z: card.cardMesh.rotation.z,
            },
          },
          true
        );
      } else {
        this.fixOrder(card);
        this.animationsService.animateCardMovement(
          card,
          {
            position: {
              x:
                environment.playerMostRightCard.position.x -
                (this.playerCards.length - 1 - index) * 0.1,
              y: environment.playerMostRightCard.position.y,
              z:
                environment.playerMostRightCard.position.z -
                (this.playerCards.length - 1 - index) * 0.01,
            },
            rotation: {
              x: card.cardMesh.rotation.x,
              y: card.cardMesh.rotation.y,
              z: card.cardMesh.rotation.z,
            },
          },
          true
        );
      }
      card.selected = !card.selected;
    }

    this.checkSelectedCards();
  }

  public handleClickOnGroup(group: Card[]) {
    group.map((card) => (card.selected = !card.selected));
    if (group[0].selected) {
      this.animationsService.animateCardGroupSelect(group);
    } else {
      this.animationsService.animateCardGroupDeselect(
        group,
        this.tableCards.player.open
      );
    }
    this.checkSelectedCards();
  }

  public handleClickOnOpponentGroup(group: Card[]) {
    group.map((card) => (card.selected = !card.selected));
    if (group[0].selected) {
      this.animationsService.animateCardGroupSelect(group);
    } else {
      this.animationsService.animateCardGroupDeselect(group, true);
    }
    this.checkSelectedCards();
  }

  public checkSelectedCards() {
    this.gameActionHandler.removeButtons();
    const selectedCards = {
      table: {
        player: [],
        opponent: [],
      },
      hand: [],
    };

    selectedCards.hand = this.playerCards.filter((card) => card.selected);
    selectedCards.hand.forEach((card) => this.fixOrder(card));
    selectedCards.hand = selectedCards.hand.sort((a, b) => a.id - b.id);

    this.tableCards.player.groups.forEach((group) => {
      if (group.length && group[0].selected) {
        selectedCards.table.player.push(group);
      }
    });

    // TODO: if opponent has a joker in group, adding to group does not work
    this.tableCards.opponent.groups.forEach((group) => {
      if (group.length && group[0].selected) {
        selectedCards.table.opponent.push(group);
      }
    });

    // the player can open up cards if its value exceeds 51
    if (
      !this.tableCards.player.open &&
      this.cardValueOnTable >= 51 &&
      this.turnState.current === "inTurn"
    ) {
      this.appendOpenButton();
    }

    // the player can hide cards to see the table if no card is selected
    if (
      !this.hasSelectedCardOnTable(selectedCards.table) &&
      !selectedCards.hand.length
    ) {
      if (this.cardsHidden) {
        this.appendShowCardsButton();
      } else {
        this.appendHideCardsButton();
        this.appendOrderCardsButton();
      }
    }

    // only one card is selected in hand and none from the table
    if (
      !this.hasSelectedCardOnTable(selectedCards.table) &&
      selectedCards.hand.length === 1
    ) {
      const card: Card = selectedCards.hand[0];
      const index: number = this.playerCards.indexOf(card);

      // move left
      if (index > 0 && index <= this.playerCards.length - 1) {
        this.appendMoveLeftButton(card);
      }

      // move right
      if (index >= 0 && index < this.playerCards.length - 1) {
        this.appendMoveRightButton(card);
      }

      // throw card
      if (this.turnState.current === "inTurn" && card.suit !== "joker") {
        this.appendThrowButton(card);
      }
    }

    // can put down on table
    if (
      !this.hasSelectedCardOnTable(selectedCards.table) &&
      selectedCards.hand.length >= 3
    ) {
      // if selected cards contain any joker, apply different rules
      if (selectedCards.hand.some((card) => card.suit === "joker")) {
        // cards can only be put down it there's only one joker selected
        if (
          selectedCards.hand.filter((card) => card.suit === "joker").length ===
          1
        ) {
          this.checkSelectedCardsOrderWithJoker(selectedCards.hand);
        }
      }

      // else if no joker is within the selected cards, apply simple rules
      else if (this.checkSelectedCardsOrderSimple(selectedCards.hand)) {
        this.appendPutDownButton(selectedCards.hand);
      }
    }

    if (
      // can add card to own group on table
      (selectedCards.table.player.length === 1 &&
        !selectedCards.table.opponent.length &&
        selectedCards.hand.length === 1) || // OR
      // can add to any group on table if open
      (this.tableCards.player.open &&
        this.XOR(
          selectedCards.table.player.length,
          selectedCards.table.opponent.length
        ) &&
        selectedCards.hand.length === 1)
    ) {
      this.handleCanAddCardToGroup(selectedCards);
    }

    // one other case of replacing a joker:
    // if the player wants to get the joker from a "same-order" group
    // it only can be done with replacing it with the other half of the group
    if (
      this.tableCards.player.open &&
      this.XOR(
        selectedCards.table.player.length,
        selectedCards.table.opponent.length
      ) &&
      selectedCards.hand.length === 2
    ) {
      const tableCards = selectedCards.table.player.length
        ? selectedCards.table.player
        : selectedCards.table.opponent;
      // this.handleReplaceJokerWithTwoCards(tableCards, selectedCards.hand);
    }

    // TODO: can merge own groups on table (only if not open)

    // can pick up group(s) from table
    if (
      selectedCards.table.player.length === 1 &&
      !selectedCards.hand.length &&
      !this.tableCards.player.open
    ) {
      this.appendPickUpGroupButton(selectedCards.table.player[0]);
    }
  }

  public sortCardsByOrder(cards: Card[]) {
    cards = cards.sort((a, b) => a.order - b.order);
  }

  // brute force the joker value end check if it can be put down by simple rules
  private checkSelectedCardsOrderWithJoker(
    selectedCards: Card[],
    sameGroup: boolean = false
  ) {
    // TODO: extend this function
    // cause: if joker is added to a group, putting down card does not work properly
    const noneJokerCards = selectedCards.filter(
      (card) => card.suit !== "joker"
    );
    const joker = selectedCards.find((card) => card.suit === "joker");
    joker.joker = true;

    const suit = noneJokerCards[0].suit;
    if (noneJokerCards.every((card) => card.suit === suit)) {
      joker.suit = suit;
      for (let i = 0; i < 13; i++) {
        if (!noneJokerCards.some((card) => card.order === i)) {
          const copyCard = cards.find(
            (card) => card.suit === suit && card.order === i
          );
          joker.order = copyCard.order;
          joker.value = copyCard.value;
          if (this.checkSelectedCardsOrderSimple(selectedCards)) {
            const jokerValues = {
              suit: joker.suit,
              value: joker.value,
              order: joker.order,
            };
            this.appendPutDownButton(
              selectedCards,
              `(Joker: ${environment.cardOrder[jokerValues.order % 13]})`,
              jokerValues,
              sameGroup
            );
          }
        }
      }
    }

    const order = noneJokerCards[0].order;
    if (noneJokerCards.every((card) => card.order === order)) {
      const jokerValues = {
        order,
        value: noneJokerCards[0].value,
      };
      joker.order = order;
      if (this.checkSelectedCardsOrderSimple(selectedCards)) {
        this.appendPutDownButton(selectedCards, "", jokerValues, sameGroup);
      }
    }
  }

  private checkSelectedCardsOrderSimple(cards: Card[]): boolean {
    let canPutDown: boolean = false;
    // cards can be put down 2 way:

    // if they are the same suit and are in order
    const suit = cards[0].suit;
    if (cards.every((card) => card.suit === suit)) {
      this.sortCardsByOrder(cards); // cards need to be sorted in case of a joker is selected

      // this while helps to put card in order in case of overflow
      while (cards[0].order === cards[cards.length - 1].order - 12) {
        cards[0].order += 13; // this overflow has to be fixed in DESELECT or in PICK UP from table events
        this.sortCardsByOrder(cards);
      }

      // check if cards are in order
      let currentInOrder = cards[0].order;
      for (let i = 0; i < cards.length - 1; i++) {
        if (
          cards[i].order === currentInOrder &&
          cards[i + 1].order === currentInOrder + 1
        ) {
          currentInOrder++;
        }
      }

      if (currentInOrder === cards[cards.length - 1].order) {
        canPutDown = true;
      }
    }

    // or if they are the same order from a different suit
    const order = cards[0].order;
    if (cards.every((card) => card.order === order) && cards.length <= 4) {
      canPutDown = cards.every((card) => {
        const currentSuit = card.suit;
        return cards.filter((card) => card.suit === currentSuit).length === 1;
      });
    }

    return canPutDown;
  }

  // TODO: use this function when pick up from table function is ready
  private fixOrder(card: Card) {
    if (card.joker) {
      card.suit = "joker";
      card.order = -1;
      card.value = -1;
    } else {
      card.order %= 13;
    }
  }

  private sortCardsById(cards: Card[]) {
    cards = cards.sort((a, b) => a.id - b.id);
  }

  private hasSelectedCardOnTable(selectedCardsOnTable: {
    player: Array<Card[]>;
    opponent: Array<Card[]>;
  }): boolean {
    return selectedCardsOnTable.player.length ||
      selectedCardsOnTable.opponent.length
      ? true
      : false;
  }

  private appendShowCardsButton() {
    // TODO: translation
    this.gameActionHandler.addButton("Show", () => {
      this.cardsHidden = false;
      this.animationsService.rePositionPlayerCards(this.playerCards, true);
      this.checkSelectedCards();
    });
  }

  private appendOrderCardsButton() {
    // TODO: translation
    this.gameActionHandler.addButton("Organize", () => {
      this.sortCardsById(this.playerCards);
      this.animationsService.rePositionPlayerCards(this.playerCards, true);
      this.checkSelectedCards();
    });
  }

  private appendHideCardsButton() {
    // TODO: translation
    this.gameActionHandler.addButton("Hide", () => {
      this.cardsHidden = true;
      this.playerCards.forEach((card) => {
        this.animationsService.animateCardMovement(
          card,
          {
            position: {
              x: card.cardMesh.position.x,
              y: card.cardMesh.position.y - 1,
              z: card.cardMesh.position.z,
            },
            rotation: {
              x: card.cardMesh.rotation.x,
              y: card.cardMesh.rotation.y,
              z: card.cardMesh.rotation.z,
            },
          },
          true
        );
      });
      this.checkSelectedCards();
    });
  }

  private appendMoveLeftButton(card: Card) {
    // TODO: translation
    this.gameActionHandler.addButton("Left", () => {
      const index: number = this.playerCards.indexOf(card);
      this.swapCards(this.playerCards, index, index - 1);
      this.animationsService.rePositionPlayerCards(this.playerCards, true);
      this.checkSelectedCards();
    });
  }

  private appendMoveRightButton(card: Card) {
    // TODO: translation
    this.gameActionHandler.addButton("Right", () => {
      const index: number = this.playerCards.indexOf(card);
      this.swapCards(this.playerCards, index, index + 1);
      this.animationsService.rePositionPlayerCards(this.playerCards, true);
      this.checkSelectedCards();
    });
  }

  private appendThrowButton(card: Card) {
    // TODO: translation
    this.gameActionHandler.addButton("Throw", () => {
      this.service.throwCard(card.id, () => {
        this.turnState.current = "endTurn";
        // TODO: translation
        $("#turn-state").text("Opponent's turn");
        card.selected = false;
        this.animationsService.animateCardMovement(
          card,
          {
            position: {
              x: environment.thrownBottomCard.position.x,
              y:
                environment.thrownBottomCard.position.y +
                this.thrown.length * 0.001,
              z: environment.thrownBottomCard.position.z,
            },
            rotation: environment.thrownBottomCard.rotation,
          },
          true
        );
        this.thrown.push(card);
        this.removeCardFromHand(card);
        this.animationsService.rePositionPlayerCards(this.playerCards, true);
        this.checkSelectedCards();
      });
    });
  }

  private removeCardFromHand(card) {
    this.playerCards.splice(this.playerCards.indexOf(card), 1);
  }

  private swapCards(cards: Card[], i: number, j: number) {
    const temp = cards[i];
    cards[i] = cards[j];
    cards[j] = temp;
  }

  private appendPutDownButton(
    cards: Card[],
    extraText?: string,
    jokerValues?: any,
    sameGroup: boolean = false
  ) {
    // TODO: translation
    this.gameActionHandler.addButton(
      `Put to table ${extraText ? extraText : ""}`,
      () => {
        const joker = cards.find((card) => card.joker);
        if (jokerValues) {
          joker.value = jokerValues.value;
          joker.order = jokerValues.order;
        }
        this.sortCardsByOrder(cards);
        if (sameGroup) {
          const group = this.tableCards.player.groups
            .filter((group) => group.length)
            .find((tableCards) => cards[0].id === tableCards[0].id);
          this.service.addCardToGroup(
            group.map((card) => card.id),
            joker.id,
            () => {
              const index = this.tableCards.player.groups.indexOf(group);

              this.tableCards.player.groups[index].push(joker);
              this.removeCardFromHand(joker);
              this.animationsService.animatePlayerTableCardGroup(
                cards,
                index,
                this.tableCards.player.open
              );
              cards.forEach((card) => {
                card.selected = false;
              });
              this.calculateValueOfTableCards();
              this.checkSelectedCards();
              this.animationsService.rePositionPlayerCards(
                this.playerCards,
                true
              );
            }
          );
        } else {
          this.service.putDownGroup(
            cards.map((card) => card.id),
            () => {
              const index = this.tableCards.player.groups.indexOf(
                this.tableCards.player.groups.find((group) => !group.length)
              );
              this.tableCards.player.groups[index] = cards;
              this.animationsService.animatePlayerTableCardGroup(
                cards,
                index,
                this.tableCards.player.open
              );
              cards.forEach((card) => {
                card.selected = false;
                this.removeCardFromHand(card);
              });
              this.calculateValueOfTableCards();
              this.checkSelectedCards();
              this.animationsService.rePositionPlayerCards(
                this.playerCards,
                true
              );
            }
          );
        }
      }
    );
  }

  private appendPickUpGroupButton(cards: Card[]) {
    // TODO: translation
    this.gameActionHandler.addButton("Pick up", () => {
      const group = this.tableCards.player.groups
        .filter((group) => group.length)
        .find((group) => group[0].id === cards[0].id);
      this.service.pickUpGroup(
        group.map((card) => card.id),
        () => {
          const index = this.tableCards.player.groups.indexOf(group);
          group.forEach((card) => {
            card.selected = false;
            this.playerCards.push(card);
          });
          this.tableCards.player.groups[index] = [];
          this.calculateValueOfTableCards();
          this.checkSelectedCards();
          this.animationsService.rePositionPlayerCards(this.playerCards, true);
        }
      );
    });
  }

  private appendOpenButton() {
    // TODO: translation
    this.gameActionHandler.addButton("Open", () => {
      this.service.openCards(() => {
        this.tableCards.player.open = true;
        this.tableCards.player.groups.forEach((group, index) => {
          if (group.length) {
            group.map((card) => (card.selected = false));
            this.animationsService.animateCardGroupOpen(group, index);
          }
        });
        this.checkSelectedCards();
      });
    });
  }

  private handleReplaceJokerWithTwoCards(
    tableCards: Card[],
    selectedCards: Card[]
  ) {
    // TODO: debug this
    const joker = tableCards.find((card) => card.joker);
    const jokerlessGroup = tableCards.filter((card) => card.suit !== "joker");
    const order = jokerlessGroup[0].order;
    if (
      selectedCards.every((card) => !card.joker && card.order === order) &&
      jokerlessGroup.every((card) => card.order === order)
    ) {
      // TODO: translation
      this.gameActionHandler.addButton("Replace Joker", () => {
        this.service.replaceJokerInGroup(
          tableCards.map((card) => card.id),
          selectedCards.map((card) => card.id),
          () => {
            tableCards.splice(tableCards.indexOf(joker), 1);
            selectedCards.forEach((card) => {
              tableCards.push(card);
              this.removeCardFromHand(card);
            });
            this.playerCards.push(joker);
            this.sortCardsByOrder(tableCards);
            tableCards.forEach((card) => {
              card.selected = false;
            });
            joker.selected = false;
            const index =
              this.tableCards.player.groups.indexOf(tableCards) !== -1
                ? this.tableCards.player.groups.indexOf(tableCards)
                : this.tableCards.opponent.groups.indexOf(tableCards);
            if (this.tableCards.player.groups.indexOf(tableCards) !== -1) {
              this.animationsService.animatePlayerTableCardGroup(
                tableCards,
                index,
                this.tableCards.player.open
              );
            } else {
              this.animationsService.animateOpponentTableCardGroup(
                tableCards,
                index,
                this.tableCards.player.open
              );
            }
            this.animationsService.rePositionPlayerCards(
              this.playerCards,
              true
            );
            this.calculateValueOfTableCards();
            this.checkSelectedCards();
          }
        );
      });
    }
  }

  private handleCanAddCardToGroup(selectedCards: any) {
    const group =
      selectedCards.table.player[0] || selectedCards.table.opponent[0];
    const card = selectedCards.hand[0];
    const newGroup = [];
    group.forEach((card) => {
      newGroup.push(card);
    });
    newGroup.push(card);
    // there are 3 cases

    // first: if the player wants to add a card to an existing group
    // TODO: edge case; if the player is not open,
    // it should be possible the replace joker in a same-order group
    if (card.suit !== "joker" && this.checkSelectedCardsOrderSimple(newGroup)) {
      this.appendAddCardToGroupButton(group, card);
    }

    // second: if the player wants to add a joker to an existing group
    if (card.suit === "joker" && !this.isJokerInGroup(group)) {
      this.checkSelectedCardsOrderWithJoker(newGroup, true);
    }

    // third: if the player wants to replace the joker in a group
    if (this.isJokerInGroup(group) && card.suit !== "joker") {
      const joker = group.find((card) => card.joker);
      if (joker.suit === card.suit && joker.order % 13 === card.order % 13) {
        this.appendReplaceJokerButton(group, card, joker);
      }
    }
  }

  private appendAddCardToGroupButton(group: Card[], card: Card) {
    // TODO: translation
    this.gameActionHandler.addButton("Add to group", () => {
      this.service.addCardToGroup(
        group.map((card) => card.id),
        card.id,
        () => {
          group.push(card);
          this.sortCardsByOrder(group);
          const index =
            this.tableCards.player.groups.indexOf(group) !== -1
              ? this.tableCards.player.groups.indexOf(group)
              : this.tableCards.opponent.groups.indexOf(group);
          this.removeCardFromHand(card);
          if (this.tableCards.player.groups.indexOf(group) !== -1) {
            this.animationsService.animatePlayerTableCardGroup(
              group,
              index,
              this.tableCards.player.open
            );
          } else {
            this.animationsService.animateOpponentTableCardGroup(
              group,
              this.tableCards.opponent.groups.indexOf(group),
              true
            );
          }
          group.forEach((card) => {
            card.selected = false;
          });
          this.calculateValueOfTableCards();
          this.checkSelectedCards();
          this.animationsService.rePositionPlayerCards(this.playerCards, true);
        }
      );
    });
  }

  private appendReplaceJokerButton(group: Card[], card: Card, joker: Card) {
    // TODO: translation
    this.gameActionHandler.addButton("Replace Joker", () => {
      this.service.replaceJokerInGroup(
        group.map((card) => card.id),
        [card.id],
        () => {
          group.splice(group.indexOf(joker), 1);
          group.push(card);
          this.removeCardFromHand(card);
          this.playerCards.push(joker);
          this.sortCardsByOrder(group);
          group.forEach((card) => {
            card.selected = false;
          });
          joker.selected = false;
          const index =
            this.tableCards.player.groups.indexOf(group) !== -1
              ? this.tableCards.player.groups.indexOf(group)
              : this.tableCards.opponent.groups.indexOf(group);
          if (this.tableCards.player.groups.indexOf(group) !== -1) {
            this.animationsService.animatePlayerTableCardGroup(
              group,
              index,
              this.tableCards.player.open
            );
          } else {
            this.animationsService.animateOpponentTableCardGroup(
              group,
              index,
              this.tableCards.player.open
            );
          }
          this.animationsService.rePositionPlayerCards(this.playerCards, true);
          this.calculateValueOfTableCards();
          this.checkSelectedCards();
        }
      );
    });
  }

  private isJokerInGroup(group: Card[]): boolean {
    return group.some((card) => card.joker);
  }

  private calculateValueOfTableCards() {
    this.cardValueOnTable = 0;
    this.tableCards.player.groups.forEach((group) => {
      group.forEach((card) => {
        this.cardValueOnTable += card.value;
      });
    });
  }

  private XOR(a: number, b: number): boolean {
    return (a === 1 && !b) || (!a && b === 1);
  }
}
