import * as $ from "jquery";

export class GameActionsComponent {
  constructor() {}

  public removeButtons() {
    $("#in-game-actions").empty();
  }

  public addButton(text: string, callback: Function) {
    const button = $(
      `<button type="button" class="btn btn-success">${text}</button>`
    );
    button.on("click", (event) => {
      callback();
      event.stopImmediatePropagation();
    });
    button.appendTo("#in-game-actions");
  }

  public appendExitButton(callback: Function) {
    $("#in-game-menu-button").empty();
    const button = $(
      `
      <button type="button" class="btn btn-success">
        <img src="/static/assets/images/exit-session.svg" />
      </button>`
    );
    button.on("click", (event) => {
      callback();
      $("#in-game-menu-button").empty();
      event.stopImmediatePropagation();
    });
    button.appendTo("#in-game-menu-button");
  }
}
