import { Camera, WebGLRenderer } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

export class CameraControlsComponent {
  public setControls(camera: Camera, renderer: WebGLRenderer) {
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enablePan = false;
    controls.enableZoom = false;
    controls.minAzimuthAngle = 0; //right
    controls.maxAzimuthAngle = Math.PI / 4; //left
    controls.minPolarAngle = (3 * Math.PI) / 8; //down
    controls.maxPolarAngle = (5 * Math.PI) / 8; // up
    controls.update();
  }
}
