import { AudioListener, AudioLoader, Audio } from "three";
import { Options } from "../common/interfaces";

export class SoundService {
  public audioListener = new AudioListener();
  public loseSound: Audio = new Audio(this.audioListener);
  public winSound: Audio = new Audio(this.audioListener);
  public cardSound: Audio = new Audio(this.audioListener);
  public requestSound: Audio = new Audio(this.audioListener);
  public mainMenuMusic: Audio = new Audio(this.audioListener);

  constructor(private options: Options) {
    const audioLoader = new AudioLoader();
    // sfx
    audioLoader.load("static/assets/audio/lose.mp3", (buffer) => {
      this.loseSound.setBuffer(buffer);
      this.loseSound.setLoop(false);
      this.loseSound.setVolume(this.options.sfx);
    });

    audioLoader.load("static/assets/audio/win.flac", (buffer) => {
      this.winSound.setBuffer(buffer);
      this.winSound.setLoop(false);
      this.winSound.setVolume(this.options.sfx);
    });

    audioLoader.load("static/assets/audio/card.wav", (buffer) => {
      this.cardSound.setBuffer(buffer);
      this.cardSound.setLoop(false);
      this.cardSound.setVolume(this.options.sfx);
    });

    audioLoader.load("static/assets/audio/request.wav", (buffer) => {
      this.requestSound.setBuffer(buffer);
      this.requestSound.setLoop(false);
      this.requestSound.setVolume(this.options.sfx);
    });

    // music
    audioLoader.load("static/assets/audio/mainmenu.mp3", (buffer) => {
      this.mainMenuMusic.setBuffer(buffer);
      this.mainMenuMusic.setLoop(true);
      this.mainMenuMusic.setVolume(this.options.music);
      this.mainMenuMusic.play();
    });
  }

  public updateVolume() {
    this.loseSound.setVolume(this.options.sfx);
    this.winSound.setVolume(this.options.sfx);
    this.cardSound.setVolume(this.options.sfx);
    this.requestSound.setVolume(this.options.sfx);
    this.mainMenuMusic.setVolume(this.options.music);
  }
}
