import {
  DoubleSide,
  CylinderGeometry,
  Mesh,
  BoxGeometry,
  MeshBasicMaterial,
  Scene,
  SphereGeometry,
} from "three";
import confetti from "canvas-confetti";

import { environment, cards } from "../common/environment";
import {
  Card,
  CardConst,
  TableCards,
  TableCardsPerPlayer,
  TurnState,
} from "../common/interfaces";
import { TextureService } from "./texture.service";
import { GameService } from "./game.service";
import { AnimationsService } from "./animations.service";
import { PlayerControlsComponent } from "./player-controls.component";
import { SoundService } from "./sound.service";

import * as $ from "jquery";

export class GameObjectComponent {
  private textureService = new TextureService();
  public deck: Card[] = [];
  private thrown: Card[] = [];
  public playerCards: Card[] = [];
  public opponentCards: Card[] = [];
  private tableCards: TableCards = {
    player: {
      open: false,
      groups: new Array([], [], [], []),
    },
    opponent: {
      open: false,
      groups: new Array([], [], [], []),
    },
  };

  private animationsService: AnimationsService = new AnimationsService(
    this.TWEEN,
    this.soundService.cardSound
  );

  private playerControls: PlayerControlsComponent = new PlayerControlsComponent(
    this.playerCards,
    this.thrown,
    this.tableCards,
    this.animationsService,
    this.service,
    this.turnState
  );

  constructor(
    private scene: Scene,
    private service: GameService,
    private TWEEN: any,
    private turnState: TurnState,
    private soundService: SoundService
  ) {}

  public initScene() {
    this.addBackground();
    this.addTable();
    this.addCardsOnTable();
    this.playerControls.handleClickOnCardInHand(null); // to add hide button
  }

  public resetScene() {
    // move every card back to deck
    this.moveCardsToDeck(this.playerCards);
    this.moveCardsToDeck(this.opponentCards);
    this.moveCardsToDeck(this.thrown);
    this.tableCards.opponent.groups.forEach((group) => {
      this.moveCardsToDeck(group);
    });
    this.tableCards.player.groups.forEach((group) => {
      this.moveCardsToDeck(group);
    });

    this.tableCards.player.open = false;
    this.tableCards.opponent.open = false;

    this.deck.forEach((card, i) => {
      card.id = null;
      card.value = null;
      card.order = null;
      card.suit = null;
      card.selected = false;
      card.cardMesh.material = this.textureService.textures.cards.blank;

      card.cardMesh.position.x = environment.deckBottomCard.position.x;
      card.cardMesh.position.y =
        environment.deckBottomCard.position.y + i * 0.005;
      card.cardMesh.position.z = environment.deckBottomCard.position.z;

      card.cardMesh.rotation.x = Math.PI / 2;
      card.cardMesh.rotation.y = 0;
      card.cardMesh.rotation.z = 0;
    });
  }

  public reshuffleThrown() {
    while (this.thrown.length) {
      const card = this.thrown.splice(0, 1)[0];
      this.deck.push(card);
      this.animationsService.animateCardMovement(
        card,
        {
          position: {
            x: environment.deckBottomCard.position.x,
            y: environment.deckBottomCard.position.y + this.deck.length * 0.005,
            z: environment.deckBottomCard.position.z,
          },
          rotation: {
            x: Math.PI / 2,
            y: 0,
            z: 0,
          },
        },
        true
      );
    }
  }

  public playerWon() {
    $("#end-game-box").css("display", "block");
    confetti({
      particleCount: 100,
      spread: 70,
      origin: { y: 0.6 },
    });
    $("#end-game-box").append("<p>You won!</p>");
    this.soundService.winSound.play();
    this.emitLeaveRoomButton();
  }

  public playerLost() {
    $("#end-game-box").css("display", "block");
    $("canvas").css("filter", "grayscale(1)");
    $("canvas").css("transition", "all 1s ease");
    $("#end-game-box").append("<p>Your opponent won!</p>");
    this.soundService.loseSound.play();
    this.emitLeaveRoomButton();
  }

  public opponentLeft() {
    $("#end-game-box").css("display", "block");
    confetti({
      particleCount: 100,
      spread: 70,
      origin: { y: 0.6 },
    });
    $("#end-game-box").append("<p>Your opponent left!</p>");
    this.soundService.winSound.play();
    this.emitLeaveRoomButton();
  }

  public fromDeckToHand(cardId: number, fast: boolean = false) {
    this.playerCards.push(this.deck[this.deck.length - 1]);
    this.deck.pop();
    const cardDetails: CardConst = cards[cardId];
    const currentCard: Card = this.playerCards[this.playerCards.length - 1];
    this.loadCardData(currentCard, cardDetails);
    this.animationsService.animateCardMovement(
      this.playerCards[this.playerCards.length - 1],
      environment.playerMostRightCard,
      fast
    );
    this.animationsService.rePositionPlayerCards(this.playerCards, fast);
  }

  public fromDeckToOpponent(fast: boolean = false) {
    this.opponentCards.push(this.deck[this.deck.length - 1]);
    this.deck.pop();
    this.animationsService.animateCardMovement(
      this.opponentCards[this.opponentCards.length - 1],
      environment.opponentMostRightCard,
      fast
    );
    this.animationsService.rePositionOpponentCards(this.opponentCards, fast);
  }

  public clickOnObject(object: Mesh) {
    // click card in hand
    if (this.checkCardInArray(this.playerCards, object)) {
      this.playerControls.handleClickOnCardInHand(object);
    }

    if (this.turnState.current === "startTurn") {
      // click card in deck
      if (this.checkCardInArray(this.deck, object)) {
        this.service.pickUpFromDeck((cardId: number) => {
          this.fromDeckToHand(cardId, true);
          this.turnState.current = "inTurn";
          this.playerControls.checkSelectedCards();
        });
      }

      // click card in thrown
      else if (this.checkCardInArray(this.thrown, object)) {
        this.service.pickUpFromThrown(() => {
          this.fromThrownToHand();
          this.turnState.current = "inTurn";
          this.playerControls.checkSelectedCards();
        });
      }
    }

    // handle click on card on table (put down by the player)
    const group = this.checkCardInGroup(this.tableCards.player, object);
    if (group) {
      this.playerControls.handleClickOnGroup(group);
    }

    // handle click on card on table (put down by the opponent, only if both players are open)
    if (this.tableCards.player.open && this.tableCards.opponent.open) {
      const opponentGroup = this.checkCardInGroup(
        this.tableCards.opponent,
        object
      );
      if (opponentGroup) {
        this.playerControls.handleClickOnOpponentGroup(opponentGroup);
      }
    }
  }

  private checkCardInArray(cardArray: Card[], object: Mesh): boolean {
    return cardArray.some((card: Card) => card.cardMesh.uuid === object.uuid);
  }

  private checkCardInGroup(
    playerCards: TableCardsPerPlayer,
    object: Mesh
  ): Card[] | undefined {
    const group = playerCards.groups.find((group) =>
      group.some((card) => card.cardMesh.uuid === object.uuid)
    );
    return group;
  }

  private fromThrownToHand() {
    this.playerCards.push(this.thrown[this.thrown.length - 1]);
    this.thrown.pop();
    this.animationsService.animateCardMovement(
      this.playerCards[this.playerCards.length - 1],
      environment.playerMostRightCard,
      true
    );
    this.animationsService.rePositionPlayerCards(this.playerCards, true);
  }

  public formThrownToOpponent() {
    const card = this.thrown.pop();
    this.opponentCards.push(card);
    this.animationsService.animateCardMovement(
      this.opponentCards[this.opponentCards.length - 1],
      environment.opponentMostRightCard,
      true
    );
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
    this.removeCardDetails(card);
  }

  public opponentThrow(index: number, cardId: number) {
    const card = this.opponentCards.splice(index, 1)[0];
    const cardDetails: CardConst = cards[cardId];
    this.loadCardData(card, cardDetails);
    this.animationsService.animateCardMovement(
      card,
      {
        position: {
          x: environment.thrownBottomCard.position.x,
          y:
            environment.thrownBottomCard.position.y +
            this.thrown.length * 0.001,
          z: environment.thrownBottomCard.position.z,
        },
        rotation: environment.thrownBottomCard.rotation,
      },
      true
    );
    this.thrown.push(card);
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
    this.turnState.current = "startTurn";
  }

  public opponentPutDownCards(cardIndexes: number[], cardValues?: number[]) {
    const currentCards = [];
    cardIndexes.forEach((index) => {
      currentCards.push(this.opponentCards[index]);
    });
    const groupIndex = this.tableCards.opponent.groups.indexOf(
      this.tableCards.opponent.groups.find((group) => !group.length)
    );
    currentCards.forEach((card, index) => {
      if (cardValues) {
        this.loadCardData(card, cards[cardValues[index]]);
      }
      this.tableCards.opponent.groups[groupIndex].push(card);
      this.opponentCards.splice(this.opponentCards.indexOf(card), 1);
    });

    // if there is a joker in the group, make it work with the group
    if (currentCards.some((card) => card.value === -1)) {
      this.applyJokerToGroup(currentCards);
    }

    this.animationsService.animateOpponentTableCardGroup(
      currentCards,
      groupIndex,
      this.tableCards.opponent.open
    );
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
  }

  public opponentPickUpCardsFormTable(groupLength: number) {
    const cards = this.tableCards.opponent.groups.find(
      (group) => group.length === groupLength
    );
    cards.forEach((card) => {
      this.opponentCards.push(card);
    });
    this.tableCards.opponent.groups[
      this.tableCards.opponent.groups.indexOf(cards)
    ] = [];
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
  }

  public opponentAddCardToGroupClosed(groupLength: number, index: number) {
    const group = this.tableCards.opponent.groups.find(
      (group) => group.length === groupLength
    );
    const card = this.opponentCards.splice(index, 1)[0];
    group.push(card);
    const groupIndex = this.tableCards.opponent.groups.indexOf(group);

    this.animationsService.animateOpponentTableCardGroup(
      group,
      groupIndex,
      false
    );
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
  }

  public opponentOpenCards(opponentTableCards: Array<number[]>) {
    const remainingGroups = opponentTableCards;
    this.tableCards.opponent.open = true;
    this.tableCards.opponent.groups.forEach((group, index) => {
      const currentGroup = remainingGroups.find(
        (cards) => cards.length === group.length
      );
      remainingGroups.splice(remainingGroups.indexOf(currentGroup), 1);
      group.forEach((cardObject, cardIndex) => {
        this.loadCardData(cardObject, cards[currentGroup[cardIndex]]);
      });

      // if group has a joker, get its position in group
      if (group.some((card) => card.order === -1)) {
        this.applyJokerToGroup(group);
      }

      this.animationsService.animateOpponentCardGroupOpen(group, index);
    });
  }

  public opponentAddCardToGroupOpen(group: number[], indexCardInHand: number) {
    const playerGroup = this.tableCards.player.groups
      .filter((tableGroup) => tableGroup.length)
      .find((tableGroup) => tableGroup[0].id === group[0]);
    const opponentGroup = this.tableCards.opponent.groups
      .filter((tableGroup) => tableGroup.length)
      .find((tableGroup) => tableGroup[0].id === group[0]);
    const card = this.opponentCards.splice(indexCardInHand, 1)[0];
    if (playerGroup) {
      const newCardId = group.find(
        (cardId) => !playerGroup.find((playerCard) => playerCard.id === cardId)
      );
      this.loadCardData(card, cards[newCardId]);
      const groupIndex = this.tableCards.player.groups.indexOf(playerGroup);
      playerGroup.splice(group.indexOf(newCardId), 0, card);
      this.playerControls.sortCardsByOrder(playerGroup);
      this.animationsService.animatePlayerTableCardGroup(
        playerGroup,
        groupIndex,
        true
      );
    } else {
      const newCardId = group.find(
        (cardId) =>
          !opponentGroup.find((opponentCard) => opponentCard.id === cardId)
      );
      this.loadCardData(card, cards[newCardId]);
      const groupIndex = this.tableCards.opponent.groups.indexOf(opponentGroup);
      opponentGroup.splice(group.indexOf(newCardId), 0, card);
      this.playerControls.sortCardsByOrder(opponentGroup);
      this.animationsService.animateOpponentTableCardGroup(
        opponentGroup,
        groupIndex,
        true
      );
    }
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
  }

  public opponentReplaceJoker(
    group: number[],
    cardIndexes: number[],
    cardIds: number[]
  ) {
    const playerGroup = this.tableCards.player.groups
      .filter((tableGroup) => tableGroup.length)
      .find((tableGroup) => tableGroup[0].id === group[0]);
    const opponentGroup = this.tableCards.opponent.groups
      .filter((tableGroup) => tableGroup.length)
      .find((tableGroup) => tableGroup[0].id === group[0]);
    if (playerGroup) {
      const joker = playerGroup.find((card) => card.joker);
      playerGroup.splice(playerGroup.indexOf(joker), 1);
      this.opponentCards.push(joker);
      cardIds.forEach((cardId, i) => {
        const cardObject = this.opponentCards.splice(cardIndexes[i], 1)[0];
        playerGroup.push(cardObject);
        this.loadCardData(cardObject, cards[cardId]);
      });
      this.removeCardDetails(joker);
      const groupIndex = this.tableCards.player.groups.indexOf(playerGroup);
      this.animationsService.animatePlayerTableCardGroup(
        playerGroup,
        groupIndex,
        true
      );
    } else {
      const joker = opponentGroup.find((card) => card.joker);
      this.opponentCards.push(joker);
      opponentGroup.splice(opponentGroup.indexOf(joker), 1);
      cardIds.forEach((cardId, i) => {
        const cardObject = this.opponentCards.splice(cardIndexes[i], 1)[0];
        opponentGroup.push(cardObject);
        this.loadCardData(cardObject, cards[cardId]);
      });
      this.removeCardDetails(joker);
      const groupIndex = this.tableCards.opponent.groups.indexOf(opponentGroup);
      this.animationsService.animateOpponentTableCardGroup(
        opponentGroup,
        groupIndex,
        true
      );
    }
    this.animationsService.rePositionOpponentCards(this.opponentCards, true);
  }

  private loadCardData(cardObject: Card, cardDetails: CardConst) {
    cardObject.cardMesh.material = this.textureService.textures.cards[
      cardDetails.texture
    ];
    cardObject.id = cardDetails.id;
    cardObject.order = cardDetails.order;
    cardObject.suit = cardDetails.suit;
    cardObject.value = cardDetails.value;
  }

  private removeCardDetails(card: Card) {
    card.cardMesh.material = this.textureService.textures.cards.blank;
    card.id = null;
    card.order = null;
    card.suit = null;
    card.value = null;
  }

  private addBackground() {
    const gameSceneBackground = new Mesh(
      new SphereGeometry(12, 32, 32, 3.5, 3.7, 0.5, 2.2),
      new MeshBasicMaterial({
        map: this.textureService.textures.background,
      })
    );
    gameSceneBackground.material.side = DoubleSide;
    this.scene.add(gameSceneBackground);
  }

  private addTable() {
    const tableMesh = new Mesh(
      new CylinderGeometry(
        environment.table.size.radius,
        environment.table.size.radius,
        environment.table.size.height,
        42
      ),
      new MeshBasicMaterial({
        map: this.textureService.textures.table,
      })
    );
    tableMesh.position.x = environment.table.position.x;
    tableMesh.position.y = environment.table.position.y;
    tableMesh.position.z = environment.table.position.z;
    this.scene.add(tableMesh);
  }

  private addCardsOnTable() {
    for (let i = 0; i < 55; i++) {
      this.deck.push({
        id: null,
        value: null,
        order: null,
        suit: null,
        selected: false,
        cardMesh: new Mesh(
          new BoxGeometry(1, 1.5, 0.0001),
          this.textureService.textures.cards.blank
        ),
      });

      this.deck[i].cardMesh.rotation.x = Math.PI / 2;
      this.deck[i].cardMesh.position.x = environment.deckBottomCard.position.x;
      this.deck[i].cardMesh.position.y =
        environment.deckBottomCard.position.y + i * 0.005;
      this.deck[i].cardMesh.position.z = environment.deckBottomCard.position.z;

      this.scene.add(this.deck[i].cardMesh);
    }
  }

  private moveCardsToDeck(cards: Card[]) {
    while (cards.length) {
      this.deck.push(cards.splice(0, 1)[0]);
    }
  }

  private applyJokerToGroup(group: Card[]) {
    const joker = group.find((card) => card.order === -1);
    const jokerlessGroup = group.filter((card) => card.order !== -1);
    const suit = jokerlessGroup[0].suit;

    // if cards are the same suit, joker should be placed based on its position in group
    if (jokerlessGroup.every((card) => card.suit === suit)) {
      const jokerIndex = group.indexOf(joker);
      if (!jokerIndex) {
        joker.order = (jokerlessGroup[0].order - 1 + 13) % 13;
      } else {
        joker.order = (group[jokerIndex - 1].order + 1) % 13;
      }

      joker.suit = suit;
      joker.value = this.getCardValueByOrder(joker.order);
    }
    // otherwise it should only copy the value and order
    else {
      joker.value = jokerlessGroup[0].value;
      joker.order = jokerlessGroup[0].order;
    }
    joker.joker = true;
  }

  private getCardValueByOrder(order: number): number {
    const values: number[] = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 10];
    return values[order];
  }

  private emitLeaveRoomButton() {
    const button = $(
      `<button type="button" class="btn btn-success">${"Return to lobby"}</button>`
    );
    button.on("click", (event) => {
      this.service.returnToLobby();
      $("#end-game-box").css("display", "none");
      $("#end-game-box").empty();
      event.stopImmediatePropagation();
    });
    button.appendTo("#end-game-box");
  }
}
