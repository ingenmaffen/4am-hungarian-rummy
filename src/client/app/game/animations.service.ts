import { Mesh, Audio } from "three";
import { environment } from "../common/environment";
import { Card, CardPosition, Dimension } from "../common/interfaces";

export class AnimationsService {
  constructor(private TWEEN: any, private cardSound: Audio) {}

  public animateCardMovement(
    fromCard: Card,
    toCard: CardPosition,
    fast: boolean = true
  ) {
    const coords = {
      x: fromCard.cardMesh.position.x,
      y: fromCard.cardMesh.position.y,
      z: fromCard.cardMesh.position.z,
      rx: fromCard.cardMesh.rotation.x,
      ry: fromCard.cardMesh.rotation.y,
      rz: fromCard.cardMesh.rotation.z,
    };
    new this.TWEEN.Tween(coords)
      .to(
        {
          x: toCard.position.x,
          y: toCard.position.y,
          z: toCard.position.z,
          rx: toCard.rotation.x,
          ry: toCard.rotation.y,
          rz: toCard.rotation.z,
        },
        fast ? 1000 : 2000
      )
      .easing(this.TWEEN.Easing.Quadratic.InOut)
      .onUpdate((coords) => {
        if (coords) {
          fromCard.cardMesh.position.x = coords.x;
          fromCard.cardMesh.position.y = coords.y;
          fromCard.cardMesh.position.z = coords.z;
          fromCard.cardMesh.rotation.x = coords.rx;
          fromCard.cardMesh.rotation.y = coords.ry;
          fromCard.cardMesh.rotation.z = coords.rz;
        }
      })
      .start();
    this.cardSound.play();
  }

  public rePositionPlayerCards(playerCards: Card[], fast: boolean) {
    for (let i = playerCards.length - 1; i >= 0; i--) {
      const count = playerCards.length - (i + 1);
      const coords = {
        x: playerCards[i].cardMesh.position.x,
        y: playerCards[i].cardMesh.position.y,
        z: playerCards[i].cardMesh.position.z,
        rx: playerCards[i].cardMesh.rotation.x,
        ry: playerCards[i].cardMesh.rotation.y,
        rz: playerCards[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          playerCards[i].selected
            ? Object.assign(
                this.getSelectedCardPosition(
                  playerCards,
                  i,
                  playerCards[i].cardMesh,
                  environment.playerMostRightCard.rotation.z +
                    (count * Math.PI) / 24
                ),
                {
                  rx: environment.playerMostRightCard.rotation.x,
                  ry: environment.playerMostRightCard.rotation.y,
                  rz:
                    environment.playerMostRightCard.rotation.z +
                    (count * Math.PI) / 24,
                }
              )
            : {
                x: environment.playerMostRightCard.position.x - count * 0.1,
                y: environment.playerMostRightCard.position.y,
                z: environment.playerMostRightCard.position.z - count * 0.01,
                rx: environment.playerMostRightCard.rotation.x,
                ry: environment.playerMostRightCard.rotation.y,
                rz:
                  environment.playerMostRightCard.rotation.z +
                  (count * Math.PI) / 24,
              },
          fast ? 1000 : 2000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords && playerCards[i]) {
            playerCards[i].cardMesh.position.x = coords.x;
            playerCards[i].cardMesh.position.y = coords.y;
            playerCards[i].cardMesh.position.z = coords.z;
            playerCards[i].cardMesh.rotation.x = coords.rx;
            playerCards[i].cardMesh.rotation.y = coords.ry;
            playerCards[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }

  public rePositionOpponentCards(opponentCards: Card[], fast: boolean) {
    for (let i = opponentCards.length - 1; i >= 0; i--) {
      const count: number = opponentCards.length - (i + 1);
      const coords = {
        x: opponentCards[i].cardMesh.position.x,
        y: opponentCards[i].cardMesh.position.y,
        z: opponentCards[i].cardMesh.position.z,
        rx: opponentCards[i].cardMesh.rotation.x,
        ry: opponentCards[i].cardMesh.rotation.y,
        rz: opponentCards[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x: environment.opponentMostRightCard.position.x - count * 0.1,
            y: environment.opponentMostRightCard.position.y - count * 0.01,
            z: environment.opponentMostRightCard.position.z + count * 0.01,
            rx: environment.opponentMostRightCard.rotation.x,
            ry: environment.opponentMostRightCard.rotation.y,
            rz:
              environment.opponentMostRightCard.rotation.z -
              (count * Math.PI) / 36,
          },
          fast ? 1000 : 2000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords && opponentCards[i]) {
            opponentCards[i].cardMesh.position.x = coords.x;
            opponentCards[i].cardMesh.position.y = coords.y;
            opponentCards[i].cardMesh.position.z = coords.z;
            opponentCards[i].cardMesh.rotation.x = coords.rx;
            opponentCards[i].cardMesh.rotation.y = coords.ry;
            opponentCards[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
    }
  }

  /*
   * This function helps to get the position of the selected card in hand (it is moved upwards, but in an angle)
   * customRz is in only needed, when the player is moving the card left or right, because
   * at that point the card's z rotation is different from what should be and position is calculation
   * heavily uses the z rotation of the card.
   * (object's z rotation by default, but overwritten in case of card movement in hand)
   */
  public getSelectedCardPosition(
    playerCards: Card[],
    index: number,
    object: Mesh,
    customRz: number = object.rotation.z
  ): Dimension {
    // these calculations are used to set the offset of the card's movement on the z-axis
    let distanceFromZeroRotation: number = playerCards.length - (index + 1) - 4;
    let offset: number =
      Math.abs(Math.sin(customRz) * (customRz / (Math.PI / 2))) + 0.01;
    offset = offset > 0.7 ? offset + 0.02 : offset;
    offset = offset > 0.8 ? offset + 0.02 : offset;
    offset =
      distanceFromZeroRotation < 0
        ? offset + distanceFromZeroRotation * 0.02
        : offset;

    return {
      x:
        environment.playerMostRightCard.position.x -
        (playerCards.length - 1 - index) * 0.1 -
        Math.sin(customRz) * 0.5,
      y: environment.playerMostRightCard.position.y + Math.cos(customRz) * 0.5,
      z:
        environment.playerMostRightCard.position.z +
        Math.cos(object.rotation.x) *
          ((1.5 * Math.sin(object.rotation.x) + Math.cos(customRz)) /
            Math.sin(object.rotation.x)) *
          0.75 -
        offset,
    };
  }

  public animatePlayerTableCardGroup(
    cards: Card[],
    groupNumber: number,
    open: boolean
  ) {
    for (let i = 0; i < cards.length; i++) {
      const coords = {
        x: cards[i].cardMesh.position.x,
        y: cards[i].cardMesh.position.y,
        z: cards[i].cardMesh.position.z,
        rx: cards[i].cardMesh.rotation.x,
        ry: cards[i].cardMesh.rotation.y,
        rz: cards[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x:
              environment.playerGroupsMostLeftCard[groupNumber].position.x +
              i * 0.1,
            y:
              environment.playerGroupsMostLeftCard[groupNumber].position.y +
              i * 0.01,
            z:
              environment.playerGroupsMostLeftCard[groupNumber].position.z +
              i * 0.01,
            rx: open
              ? -environment.playerGroupsMostLeftCard[groupNumber].rotation.x
              : environment.playerGroupsMostLeftCard[groupNumber].rotation.x,
            ry: environment.playerGroupsMostLeftCard[groupNumber].rotation.y,
            rz:
              environment.playerGroupsMostLeftCard[groupNumber].rotation.z +
              (i * Math.PI) / 24,
          },
          2000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords) {
            cards[i].cardMesh.position.x = coords.x;
            cards[i].cardMesh.position.y = coords.y;
            cards[i].cardMesh.position.z = coords.z;
            cards[i].cardMesh.rotation.x = coords.rx;
            cards[i].cardMesh.rotation.y = coords.ry;
            cards[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }

  public animateOpponentTableCardGroup(
    cards: Card[],
    groupNumber: number,
    open: boolean
  ) {
    for (let i = 0; i < cards.length; i++) {
      const coords = {
        x: cards[i].cardMesh.position.x,
        y: cards[i].cardMesh.position.y,
        z: cards[i].cardMesh.position.z,
        rx: cards[i].cardMesh.rotation.x,
        ry: cards[i].cardMesh.rotation.y,
        rz: cards[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x:
              environment.opponentGroupsMostRightCard[groupNumber].position.x +
              i * 0.1,
            y:
              environment.opponentGroupsMostRightCard[groupNumber].position.y +
              i * 0.01,
            z:
              environment.opponentGroupsMostRightCard[groupNumber].position.z +
              i * 0.01,
            rx: open
              ? -environment.opponentGroupsMostRightCard[groupNumber].rotation.x
              : environment.opponentGroupsMostRightCard[groupNumber].rotation.x,
            ry: environment.opponentGroupsMostRightCard[groupNumber].rotation.y,
            rz:
              environment.opponentGroupsMostRightCard[groupNumber].rotation.z -
              (i * Math.PI) / 24,
          },
          2000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords) {
            cards[i].cardMesh.position.x = coords.x;
            cards[i].cardMesh.position.y = coords.y;
            cards[i].cardMesh.position.z = coords.z;
            cards[i].cardMesh.rotation.x = coords.rx;
            cards[i].cardMesh.rotation.y = coords.ry;
            cards[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }

  public animateCardGroupSelect(group: Card[]) {
    for (let i = 0; i < group.length; i++) {
      const coords = {
        x: group[i].cardMesh.position.x,
        y: group[i].cardMesh.position.y,
        z: group[i].cardMesh.position.z,
        rx: group[i].cardMesh.rotation.x,
        ry: group[i].cardMesh.rotation.y,
        rz: group[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x: group[i].cardMesh.position.x,
            y: group[i].cardMesh.position.y + 1.5,
            z: group[i].cardMesh.position.z,
            rx: -Math.PI / 6,
            ry:
              Math.PI / 2 -
              Math.atan(
                group[i].cardMesh.position.z /
                  -Math.abs(group[i].cardMesh.position.x)
              ),
            rz: group[i].cardMesh.rotation.z, // TODO: tweak z rotation
          },
          1000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords) {
            group[i].cardMesh.position.x = coords.x;
            group[i].cardMesh.position.y = coords.y;
            group[i].cardMesh.position.z = coords.z;
            group[i].cardMesh.rotation.x = coords.rx;
            group[i].cardMesh.rotation.y = coords.ry;
            group[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }

  public animateCardGroupDeselect(group: Card[], open: boolean) {
    for (let i = 0; i < group.length; i++) {
      const coords = {
        x: group[i].cardMesh.position.x,
        y: group[i].cardMesh.position.y,
        z: group[i].cardMesh.position.z,
        rx: group[i].cardMesh.rotation.x,
        ry: group[i].cardMesh.rotation.y,
        rz: group[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x: group[i].cardMesh.position.x,
            y: group[i].cardMesh.position.y - 1.5,
            z: group[i].cardMesh.position.z,
            rx: open ? -Math.PI / 2 : Math.PI / 2,
            ry: 0,
            rz: group[i].cardMesh.rotation.z,
          },
          1000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords) {
            group[i].cardMesh.position.x = coords.x;
            group[i].cardMesh.position.y = coords.y;
            group[i].cardMesh.position.z = coords.z;
            group[i].cardMesh.rotation.x = coords.rx;
            group[i].cardMesh.rotation.y = coords.ry;
            group[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }

  public animateCardGroupOpen(group: Card[], groupNumber: number) {
    for (let i = 0; i < group.length; i++) {
      const coords = {
        x: group[i].cardMesh.position.x,
        y: group[i].cardMesh.position.y,
        z: group[i].cardMesh.position.z,
        rx: group[i].cardMesh.rotation.x,
        ry: group[i].cardMesh.rotation.y,
        rz: group[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x:
              environment.playerGroupsMostLeftCard[groupNumber].position.x +
              i * 0.1,
            y:
              environment.playerGroupsMostLeftCard[groupNumber].position.y +
              i * 0.01,
            z:
              environment.playerGroupsMostLeftCard[groupNumber].position.z +
              i * 0.01,
            rx: -environment.playerGroupsMostLeftCard[groupNumber].rotation.x,
            ry: environment.playerGroupsMostLeftCard[groupNumber].rotation.y,
            rz:
              environment.playerGroupsMostLeftCard[groupNumber].rotation.z +
              (i * Math.PI) / 24,
          },
          1000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords) {
            group[i].cardMesh.position.x = coords.x;
            group[i].cardMesh.position.y = coords.y;
            group[i].cardMesh.position.z = coords.z;
            group[i].cardMesh.rotation.x = coords.rx;
            group[i].cardMesh.rotation.y = coords.ry;
            group[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }

  public animateOpponentCardGroupOpen(group: Card[], groupNumber: number) {
    for (let i = 0; i < group.length; i++) {
      const coords = {
        x: group[i].cardMesh.position.x,
        y: group[i].cardMesh.position.y,
        z: group[i].cardMesh.position.z,
        rx: group[i].cardMesh.rotation.x,
        ry: group[i].cardMesh.rotation.y,
        rz: group[i].cardMesh.rotation.z,
      };
      new this.TWEEN.Tween(coords)
        .to(
          {
            x:
              environment.opponentGroupsMostRightCard[groupNumber].position.x +
              i * 0.1,
            y:
              environment.opponentGroupsMostRightCard[groupNumber].position.y +
              i * 0.01,
            z:
              environment.opponentGroupsMostRightCard[groupNumber].position.z +
              i * 0.01,
            rx: -environment.opponentGroupsMostRightCard[groupNumber].rotation
              .x,
            ry: environment.opponentGroupsMostRightCard[groupNumber].rotation.y,
            rz:
              environment.opponentGroupsMostRightCard[groupNumber].rotation.z +
              (i * Math.PI) / 24,
          },
          1000
        )
        .easing(this.TWEEN.Easing.Quadratic.InOut)
        .onUpdate((coords) => {
          if (coords) {
            group[i].cardMesh.position.x = coords.x;
            group[i].cardMesh.position.y = coords.y;
            group[i].cardMesh.position.z = coords.z;
            group[i].cardMesh.rotation.x = coords.rx;
            group[i].cardMesh.rotation.y = coords.ry;
            group[i].cardMesh.rotation.z = coords.rz;
          }
        })
        .start();
      this.cardSound.play();
    }
  }
}
