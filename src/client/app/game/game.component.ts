import {
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  Raycaster,
  Vector2,
  Mesh,
} from "three";

import { CameraControlsComponent } from "./camera-controls.component";
import { GameObjectComponent } from "./game-objects.component";
import { GameService } from "./game.service";
import { TurnState, Options } from "../common/interfaces";
import { GameEventHandlerComponent } from "./game-event-handler.component";
import { SoundService } from "./sound.service";

import * as TWEEN from "@tweenjs/tween.js";

export class GameComponent {
  private renderer: WebGLRenderer;
  private scene: Scene;
  private camera: PerspectiveCamera;
  private topCamera: PerspectiveCamera;

  private tween = TWEEN["default"];

  private raycaster = new Raycaster();
  private mouse = new Vector2();

  private clickedObject: any;

  private gameService: GameService;
  private gameObjectComponent: GameObjectComponent;

  private soundService: SoundService;

  public isTopView: boolean = false;

  constructor(private socket: any, public options: Options) {
    this.scene = new Scene();

    const WIDTH = window.innerWidth;
    const HEIGHT = window.innerHeight;
    const aspectRatio = WIDTH / HEIGHT;

    // set up renderer
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setSize(WIDTH, HEIGHT);
    document.body.appendChild(this.renderer.domElement);

    // The camera uses orbit controls so it is positioned away from the center
    this.camera = new PerspectiveCamera(55, aspectRatio, 0.1, 15);
    this.camera.position.z = -0.01;
    this.camera.position.y = 0.01;
    this.camera.position.x = -0.01;

    this.topCamera = new PerspectiveCamera(55, aspectRatio, 0.1, 15);
    this.topCamera.position.z = -5.5;
    this.topCamera.position.y = 5;
    this.topCamera.position.x = -2.5;
    this.topCamera.rotation.x = -Math.PI / 2;

    // add camera controls
    const _camerControls = new CameraControlsComponent().setControls(
      this.camera,
      this.renderer
    );

    // call first render
    this.render(this.renderer);

    window.addEventListener("resize", () => {
      this.handleWindowResize(this.renderer, this.camera);
    });

    window.addEventListener("mousedown", (event) => {
      if ((event.target as Element).nodeName === "CANVAS") {
        this.getMousePosition(event);
        this.clickedObject = this.clickOnObject();
      }
    });

    window.addEventListener("mouseup", (event) => {
      this.getMousePosition(event);
      if (this.clickedObject === this.clickOnObject() && this.gameObjectComponent) {
        this.gameObjectComponent.clickOnObject(this.clickedObject);
      }
      this.clickedObject = null;
    });

    window.addEventListener("keydown", (event) => {
      if (event.code === "Space") {
        this.isTopView = !this.isTopView;
        const background = this.scene.children.find(
          (mesh: Mesh) => mesh.geometry.type === "SphereGeometry"
        );

        if (background) {
          background.visible = !this.isTopView;
        }
      }
    });
  }

  public resetGameScene(roomId: string) {
    this.gameService.roomId = roomId;
    this.gameObjectComponent.resetScene();
  }

  public initiateGameSceneWithSounds() {
    this.soundService = new SoundService(this.options);
    this.camera.add(this.soundService.audioListener);
    const turnState: TurnState = {
      current: null,
    };
    this.gameService = new GameService(this.socket, null);
    this.gameObjectComponent = new GameObjectComponent(
      this.scene,
      this.gameService,
      this.tween,
      turnState,
      this.soundService
    );
    this.gameObjectComponent.initScene();

    const _gameEvenTHandler: GameEventHandlerComponent = new GameEventHandlerComponent(
      this.socket,
      turnState,
      this.gameObjectComponent,
      this.soundService
    );
  }

  public updateVolume() {
    this.soundService.updateVolume();
  }

  private handleWindowResize(
    renderer: WebGLRenderer,
    camera: PerspectiveCamera
  ) {
    const HEIGHT = window.innerHeight;
    const WIDTH = window.innerWidth;
    const aspectRatio = WIDTH / HEIGHT;
    renderer.setSize(WIDTH, HEIGHT);
    camera.aspect = aspectRatio;
    camera.updateProjectionMatrix();
  }

  private render(renderer: WebGLRenderer) {
    requestAnimationFrame(() => {
      this.render(renderer);
    });
    this.renderer.autoClear = false;
    this.renderer.clear();
    this.renderer.render(
      this.scene,
      this.isTopView ? this.topCamera : this.camera
    );
    this.tween.update();
  }

  private getMousePosition(event) {
    let x: number = 0;
    let y: number = 0;
    if (event.changedTouches) {
      x = event.changedTouches[0].pageX;
      y = event.changedTouches[0].pageY;
    } else if (event.targetTouches) {
      x = event.targetTouches[0].pageX;
      y = event.targetTouches[0].pageY;
    } else {
      x = event.clientX;
      y = event.clientY;
    }
    this.mouse.x = (x / window.innerWidth) * 2 - 1;
    this.mouse.y = -(y / window.innerHeight) * 2 + 1;
  }

  private clickOnObject(): Mesh {
    this.raycaster.setFromCamera(this.mouse, this.camera);
    var intersects = this.raycaster.intersectObjects([this.scene], true);
    if (intersects.length > 0) {
      return intersects[0].object as Mesh;
    } else {
      return null;
    }
  }
}
