import { CallService } from "./common/call.service";
import { Options } from "./common/interfaces";
import { GameComponent } from "./game/game.component";
import { MainMenuComponent } from "./menu/main-menu.component";

export class AppComponent {
  private callService: CallService = new CallService();
  private socket = this.callService.getSocket();

  private shouldLoadGame: boolean = false; // TODO: false by default, true if reconnected to game (in case of connection lost)
  private options: Options = {
    music: localStorage.getItem("musicVolume")
      ? +localStorage.getItem("musicVolume")
      : 0.1,
    sfx: localStorage.getItem("sfxVolume")
      ? +localStorage.getItem("sfxVolume")
      : 0.5,
  };

  private game = new GameComponent(this.socket, this.options);
  private menu = new MainMenuComponent(this.socket, this.game);

  constructor() {
    this.socket.on("leaveRoom", () => {
      // TODO: notify player
      this.menu.leaveRoom();
    });
    if (this.shouldLoadGame) {
      // this.game.initGameScene(); // TODO: on reconnect, pass the roomId to load the game immediately
    } else {
      this.menu.appendUsernameInput();
    }
  }
}
