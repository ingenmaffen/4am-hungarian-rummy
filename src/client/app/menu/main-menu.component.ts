import * as $ from "jquery";
import { GameComponent } from "../game/game.component";
import { MenuItem } from "../common/interfaces";
import { GameActionsComponent } from "../game/actions.component";

export class MainMenuComponent {
  // TODO: translation
  mainMenuButtons: MenuItem[] = [
    {
      text: "Search for players",
      callback: () => {
        this.removeMainMenuItems();
        this.requestPlayerList();
      },
    },
    {
      text: "Options",
      callback: () => {
        this.removeMainMenuItems();
        this.loadOptions();
      },
    },
  ];

  private roomId: string;

  constructor(private socket: any, private game: GameComponent) {
    $("#in-game-actions").hide();
    this.socket.on("getRequestFrom", ({ playerId, playerName }) => {
      this.appendGameRequest(playerId, playerName);
    });

    this.socket.on("loadGame", (roomId: string) => {
      this.hideMenu();
      $("#in-game-actions").show();
      $("canvas").show();
      this.roomId = roomId;
      this.game.resetGameScene(roomId);
      this.game.isTopView = false;

      const actionHandler: GameActionsComponent = new GameActionsComponent();
      actionHandler.appendExitButton(() => {
        this.socket.emit("exitRoom", this.roomId);
        this.leaveRoom();
      });
    });

    this.socket.on("sendPlayerList", (playerList: any[]) => {
      this.removeMainMenuItems();
      this.appendPlayerListMenu();
      const otherPlayers: any[] = playerList.filter(
        (item) => item.playerId !== this.socket.id
      );
      otherPlayers.forEach((player) => {
        this.listPlayersClickable(player.playerName, () => {
          this.socket.emit("sendRequestTo", player.playerId);
        });
      });
    });

    this.socket.on("canUseName", () => {
      this.removeMainMenuItems();
      this.loadMainMenu();
    });
  }

  public appendUsernameInput() {
    const savedUsername = localStorage.getItem("username");
    const username = $(
      `<input type="text" class="form-control" placeholder="Username"/>`
    );
    username.on("keydown", (event) => {
      if (event.key === "Enter") {
        this.sendUsername(username.val());
      }
    });
    username.appendTo("#main-menu-items");
    username.trigger("focus");

    if (savedUsername) {
      username.val(savedUsername);
    }

    const confirmButton = $(
      `<button type="button" class="btn btn-success menu-button-quarter">OK</button>`
    );
    confirmButton.on("click", (_event) => {
      this.sendUsername(username.val());
    });
    confirmButton.appendTo("#main-menu-items");
  }

  public leaveRoom() {
    $("#in-game-actions").hide();
    this.reopenMenu();
    $("#in-game-menu-button").empty();
    $("canvas").hide();
    $("canvas").css("filter", "");
    $("#turn-state").text("");
  }

  private sendUsername(username) {
    localStorage.setItem("username", username);
    this.socket.emit("username", username);
    this.game.initiateGameSceneWithSounds();
  }

  private loadMainMenu() {
    this.mainMenuButtons.forEach((menuItem) => {
      const menuButton = $(
        `<button type="button" class="btn btn-success menu-button-full">${menuItem.text}</button>`
      );
      menuButton.on("click", (_event) => {
        menuItem.callback();
      });
      menuButton.appendTo("#main-menu-items");
    });
  }

  private loadOptions() {
    let sfxVolume: number = localStorage.getItem("sfxVolume")
      ? +localStorage.getItem("sfxVolume")
      : 0.5;
    let musicVolume: number = localStorage.getItem("musicVolume")
      ? +localStorage.getItem("musicVolume")
      : 0.1;
    const musicSlider = $(`
    <div>
      <div class="option-name">Music:</div>
      <input type="range" class="slider" id="music-slider" min="0" max="100" step="10" value="${
        musicVolume * 100
      }">
    </div>
  `);
    const sfxSlider = $(`
    <div>
      <div class="option-name">SFX:</div>
      <input type="range" class="slider" id="sfx-slider" min="0" max="100" step="10" value="${
        sfxVolume * 100
      }">
    </div>
  `);
    // TODO: translation
    const backButton = $(
      `<button type="button" class="btn btn-success menu-button-half">${"Back"}</button>`
    );
    backButton.on("click", (_event) => {
      this.removeMainMenuItems();
      this.loadMainMenu();
    });
    // TODO: translation
    const saveButton = $(
      `<button type="button" class="btn btn-success menu-button-half">${"Save"}</button>`
    );
    saveButton.on("click", (_event) => {
      sfxVolume = +$("#sfx-slider").val();
      musicVolume = +$("#music-slider").val();
      this.game.options.sfx = sfxVolume / 100;
      this.game.options.music = musicVolume / 100;
      localStorage.setItem("sfxVolume", (sfxVolume / 100).toString());
      localStorage.setItem("musicVolume", (musicVolume / 100).toString());
      this.removeMainMenuItems();
      this.loadMainMenu();
      this.game.updateVolume();
    });
    musicSlider.appendTo("#main-menu-items");
    sfxSlider.appendTo("#main-menu-items");
    backButton.appendTo("#main-menu-items");
    saveButton.appendTo("#main-menu-items");
  }

  private removeMainMenuItems() {
    $("#main-menu-items").empty();
  }

  private hideMenu() {
    this.removeMainMenuItems();
    $("#main-menu").hide();
  }

  private reopenMenu() {
    $("#main-menu").show();
    this.loadMainMenu();
  }

  private requestPlayerList() {
    this.socket.emit("getPlayerList", null);
  }

  private appendPlayerListMenu() {
    const list = $(`
    <div class="table-container">
      <table class="table table-dark">
        <thead>
          <tr>
            <th>Username</th>
            <th>Request</th>
          </tr>
        </thead>
        <tbody id="player-list">
        </tbody>
      </table>
    </div>`);

    list.appendTo("#main-menu-items");

    // TODO: translation
    const backButton = $(
      `<button type="button" class="btn btn-success menu-button-half">${"Back"}</button>`
    );
    backButton.on("click", (_event) => {
      this.removeMainMenuItems();
      this.loadMainMenu();
    });
    backButton.appendTo("#main-menu-items");

    // TODO: translation
    const refreshButton = $(
      `<button type="button" class="btn btn-success menu-button-half">${"Refresh"}</button>`
    );
    refreshButton.on("click", (_event) => {
      this.requestPlayerList();
    });
    refreshButton.appendTo("#main-menu-items");
  }

  private listPlayersClickable(text: string, callback: Function) {
    const player = $(
      `<tr><th class="player-list-name">${text}</th>
      <th><button class="btn btn-success player-list-button">${"Send"}</button></th></tr>`
    );
    player.on("click", (event) => {
      callback();
      event.stopImmediatePropagation();
    });
    player.appendTo("#player-list");
  }

  private appendGameRequest(playerId: string, playerName?: string) {
    $("#request-modal").remove();
    // TODO: translation
    const acceptButton = `<button id="accept-${playerId}" type="button" class="btn btn-success">${"Accept"}</button>`;
    // TODO: translation
    const cancelButton = `<button id="refuse-${playerId}" type="button" class="btn btn-danger">${"Refuse"}</button>`;
    const modal = $(
      // TODO: translation
      `
      <div id="request-modal" class="request-modal">
        <div class="header">Game Request</div>
        <div class="request-body">
          <div class="text">${
            playerName ? playerName : playerId
          } sent you a game request</div>
          <div id="request-options-${playerId}"> ${acceptButton} ${cancelButton}</div>
        </div>
      </div>
      `
    );
    modal.appendTo("#main-menu").hide().show("slow");
    $(`#accept-${playerId}`).on("click", (event) => {
      this.socket.emit("acceptRequest", playerId);
      event.stopImmediatePropagation();
      this.hideMenu();
      modal.remove();
    });

    $(`#refuse-${playerId}`).on("click", (event) => {
      this.socket.emit("cancelRequest", playerId);
      event.stopImmediatePropagation();
      modal.remove();
    });
  }
}
