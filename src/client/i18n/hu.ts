export const hu = {
  title: "4 AM Hungarian Rummy",
  name: "Név",
  copyright: "© 2018-2019 Rabi Róbert, Minden jog fenntartva.",
  cookies: {
    footer: "Adatvédelmi Szabályzat",
    policy:
      'A sütik olyan adatok, amelyket tartalmazhatnak névtelen, egyedi azonosítókat. A sütiket a webszerverer küldi és az Ön számítógépén kerülnek tárolásra. Mint sok más oldal, mi is használunk ,,sütiket" információgyűjtésre. A böngészőben beállíthatja, hogy ne tároljon sütiket vagy hogy lássa, mikor kerülnek küldésre.',
    snackbar:
      'Ez az oldal ,,sütiket" használ a beállítások mentésére. További információért olvassa el a következőt: ',
    accept: "Elfogadom",
    back: "Vissza",
  },
  music: {
    allowance: "Szeretné, hogy az oldal hangokat játsszon le?",
    yes: "Igen",
    no: "Nem",
  },
  menu: {
    singleplayer: "Egyjátékos",
    multiplayer: "Többjátékos",
    options: "Beállítások",
    tutorial: "Gyakorlás",
    rules: "Szabályok",
    credits: "Felhasznált tartalmak",
  },
  options: {
    name: "Név",
    music: "Zene",
    sfx: "Hangeffektek",
    antialiasing: "Élsimítás",
    back: "Vissza",
    save: "Mentés",
  },
  credits: {
    textures: "Textúrák",
    table: "Asztal",
    background: "Hátterek",
    lobby: "Menü",
    ingame: "Játék",
    cards: "Kártyák",
    sounds: "Hangok",
    piatnik: "Piatnik",
    piatnikurl: "http://piatnik.hu/",
    cardsound: "Kártya hang",
    music: "Zene",
    mainmenumusic: "Főmenü",
    back: "Vissza",
  },
  inGame: {
    throw: "Kidob",
    left: "Balra",
    right: "Jobbra",
  },
  placeholder: {
    inDevelopment: "Fejlesztés alatt",
  },
};
