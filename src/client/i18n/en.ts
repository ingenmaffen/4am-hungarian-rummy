export const en = {
  title: "4 AM Hungarian Rummy",
  name: "Name",
  copyright: "© 2018-2019 Rabi Róbert, All rights reserved.",
  cookies: {
    footer: "Privacy Policy",
    policy:
      'Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer\'s hard drive. Like many sites, we use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent.',
    snackbar:
      'This site uses "cookies" to save your preferences. For more information, please read the ',
    accept: "Accept",
    back: "Back",
  },
  music: {
    allowance: "Do you wish this site to play sounds?",
    yes: "Yes",
    no: "No",
  },
  menu: {
    singleplayer: "Singleplayer",
    multiplayer: "Multiplayer",
    options: "Options",
    tutorial: "Tutorial",
    rules: "Rules",
    credits: "Credits",
  },
  options: {
    name: "Name",
    music: "Music",
    sfx: "SFX",
    antialiasing: "Antialiasing",
    back: "Back",
    save: "Save",
  },
  credits: {
    textures: "Textures",
    table: "Table",
    background: "Backgrounds",
    lobby: "Lobby",
    ingame: "Ingame",
    cards: "Cards",
    piatnik: "Piatnik",
    piatnikurl: "http://www.piatnik.us/",
    sounds: "Sounds",
    cardsound: "Card flip sound",
    music: "Music",
    mainmenumusic: "Main menu",
    back: "Back",
  },
  inGame: {
    throw: "Throw",
    left: "Move left",
    right: "Move right",
  },
  placeholder: {
    inDevelopment: "In development",
  },
};
