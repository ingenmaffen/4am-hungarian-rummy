module.exports = {
  canThrow: (room, playerId, cardId) => {
    const player = getPlayerById(room, playerId);
    const otherPlayer = getOtherPlayerById(room, playerId);
    if (
      player.hand.find((card) => card === cardId) &&
      sumCardsByPlayer(player) + sumCardsByPlayer(otherPlayer) === 14 + 15 &&
      cardId < 52
    ) {
      return true;
    } else {
      return false;
    }
  },
  canPickUpThrown: (room, playerId) => {
    const player = getPlayerById(room, playerId);
    const otherPlayer = getOtherPlayerById(room, playerId);
    if (
      room.thrown.length &&
      room.thrown[room.thrown.length - 1].playerId !== playerId &&
      sumCardsByPlayer(player) + sumCardsByPlayer(otherPlayer) === 14 + 14
    ) {
      return true;
    } else {
      return false;
    }
  },
  canPickUpFromDeck: (room, playerId) => {
    const player = getPlayerById(room, playerId);
    const otherPlayer = getOtherPlayerById(room, playerId);
    if (
      room.thrown.length &&
      room.thrown[room.thrown.length - 1].playerId !== playerId &&
      sumCardsByPlayer(player) + sumCardsByPlayer(otherPlayer) === 14 + 14
    ) {
      return true;
    } else {
      return false;
    }
  },
  canPutDownGroup: (room, playerId, group) => {
    const player = getPlayerById(room, playerId);
    const otherPlayer = getOtherPlayerById(room, playerId);
    // TODO: apply rules
    return true;
  },
  canPickUpGroup: (room, playerId, group) => {
    const player = getPlayerById(room, playerId);
    return !player.open && playerHasGroup(player, group);
  },
  canAddCardToGroup: (room, playerId, group, cardId) => {
    const player = getPlayerById(room, playerId);
    const otherPlayer = getOtherPlayerById(room, playerId);
    // TODO: apply rules
    return true;
  },
  canPlayerOpen: (room, playerId) => {
    const player = getPlayerById(room, playerId);
    const otherPlayer = getOtherPlayerById(room, playerId);
    // TODO: check if player is in turn and has enough card put down
    return true;
  },
  getPlayerById: (room, playerId) => {
    if (room.player1.uuid === playerId) {
      return room.player1;
    } else {
      return room.player2;
    }
  },
  canReplaceJoker: (room, playerId) => {
    // TODO: apply rules
    return true;
  },
};

function getPlayerById(room, playerId) {
  if (room.player1.uuid === playerId) {
    return room.player1;
  } else {
    return room.player2;
  }
}

function getOtherPlayerById(room, playerId) {
  if (room.player1.uuid === playerId) {
    return room.player2;
  } else {
    return room.player1;
  }
}

function playerHasGroup(player, group) {
  let hasGroup = false;
  player.table.forEach((tableGroup) => {
    if (tableGroup.length === group.length) {
      if (JSON.stringify(tableGroup) === JSON.stringify(group)) {
        hasGroup = true;
      }
    }
  });

  return hasGroup;
}

function sumCardsByPlayer(player) {
  let count = 0;
  player.table.forEach((group) => {
    count += group.length;
  });
  count += player.hand.length;
  return count;
}
