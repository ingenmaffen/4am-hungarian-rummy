module.exports = {
  start: (http) => {
    const { v4: uuidv4 } = require("uuid");
    const io = require("socket.io")(http, { cookie: false });
    const rooms = {};
    const rules = require("./rules");
    const users = {};
    io.on("connection", (socket) => {
      console.log(`${socket.id} connected`);

      socket.on("disconnect", () => {
        console.log(`${socket.id} disconnected`);
        // TODO: a better option would be to reconnect the player
        const roomId = getRoomByPlayerId(socket.id, rooms);
        io.to(roomId).emit("opponentLeft");
        if (roomId) {
          const otherPlayerId = getOtherPlayerId(rooms[roomId], socket.id);
          const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
          if (otherPlayerSocket) {
            otherPlayerSocket.leave(roomId);
          }
        }
        delete users[socket.id];
      });

      socket.on("username", (username) => {
        users[socket.id] = username;
        socket.emit("canUseName");
      });

      // events to handle request for matching up players
      socket.on("getPlayerList", () => {
        const clients = [];
        io.sockets.sockets.forEach((client) => {
          clients.push({
            playerId: client.id,
            playerName: users[client.id],
          });
          // TODO: do not add players in that are in a game
        });
        socket.emit("sendPlayerList", clients);
      });

      // TODO: sending request not working (probably due to sockets becomming a Map)
      socket.on("sendRequestTo", (playerId) => {
        const requestedSocket = io.of("/").sockets.get(playerId);
        if (requestedSocket) {
          requestedSocket.emit("getRequestFrom", {
            playerId: socket.id,
            playerName: users[socket.id],
          });
        }
      });

      socket.on("acceptRequest", (playerId) => {
        const requestedSocket = io.of("/").sockets.get(playerId);
        if (requestedSocket) {
          const roomId = uuidv4();
          socket.join(roomId);
          requestedSocket.join(roomId);
          io.to(roomId).emit("loadGame", roomId);

          const room = require("./room").initRoom(
            roomId,
            requestedSocket.id,
            socket.id
          );
          requestedSocket.emit("initialCards", room.player1.hand);
          socket.emit("initialCards", room.player2.hand);
          rooms[roomId] = room;
        }
      });

      socket.on("cancelRequest", (playerId) => {
        // TODO: send info to player about cancelation
      });

      // in game events
      socket.on("throwCard", (resp) => {
        const cardId = resp.cardId;
        const room = rooms[resp.roomId];
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        // TODO: remove quickfix (in case of cardId is 0, rule validation is not working)
        if (rules.canThrow(room, socket.id, cardId) || cardId === 0) {
          socket.emit("canThrow");
          const player = rules.getPlayerById(room, socket.id);
          const index = player.hand.indexOf(cardId);
          player.hand.splice(index, 1);
          room.thrown.push({
            cardId,
            playerId: socket.id,
          });
          otherPlayerSocket.emit("opponentThrow", { index, cardId });

          if (!player.hand.length && player.open) {
            socket.emit("win");
            otherPlayerSocket.emit("lose");
            socket.leave(resp.roomId);
            otherPlayerSocket.leave(resp.roomId);
          }
        }
      });

      socket.on("pickUpThrownCard", (roomId) => {
        // TODO: extend rules: can only pick up if first round or can open
        const room = rooms[roomId];
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canPickUpThrown(room, socket.id)) {
          socket.emit("canPickUpFromThrown");
          const card = room.thrown.splice(room.thrown.length - 1, 1)[0];
          player.hand.push(card.cardId);
          otherPlayerSocket.emit("opponentPickUpThrown");
        }
      });

      socket.on("pickUpDeckCard", (roomId) => {
        const room = rooms[roomId];
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canPickUpFromDeck(room, socket.id)) {
          const card = room.deck.splice(0, 1)[0];
          player.hand.push(card);
          socket.emit("canPickUpFromDeck", card);
          otherPlayerSocket.emit("opponentPickUpFromDeck");

          if (!room.deck.length) {
            io.to(roomId).emit("reshuffleThrown");

            // generate cards to deck
            const deck = [];
            for (let i = 0; i < room.thrown.length; i++) {
              while (deck.length == i) {
                let rand = Math.floor(Math.random() * room.thrown.length);
                let hasSame = 0;
                for (let j = 0; j < deck.length; j++) {
                  if (deck[j] == rand) hasSame++;
                }
                if (hasSame == 0) deck.push(rand);
              }
            }

            deck.forEach((cardIndex) => {
              room.deck.push(room.thrown[cardIndex].cardId);
            });
            room.thrown = [];
          }
        }
      });

      socket.on("putDownGroup", (resp) => {
        const room = rooms[resp.roomId];
        const group = resp.group;
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canPutDownGroup(room, socket.id, group)) {
          player.table.push(group);
          const indexes = [];
          group.forEach((card) => {
            const index = player.hand.indexOf(card);
            indexes.push(index);
          });
          group.forEach((card) => {
            player.hand.splice(player.hand.indexOf(card), 1);
          });
          socket.emit("canPutDownGroup");
          if (player.open) {
            otherPlayerSocket.emit("opponentPutDownGroupOpen", {
              indexes,
              group,
            });
          } else {
            otherPlayerSocket.emit("opponentPutDownGroup", indexes);
          }
        }
      });

      socket.on("pickUpGroup", (resp) => {
        const room = rooms[resp.roomId];
        const group = resp.group;
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canPickUpGroup(room, socket.id, group)) {
          group.forEach((card) => {
            player.hand.push(card);
          });
          const groupOnTable = player.table.find(
            (tableGroup) =>
              orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
          );
          const index = player.table.indexOf(groupOnTable);
          player.table.splice(index, 1);
          socket.emit("canPickUpGroup");
          otherPlayerSocket.emit("opponentPickUpGroup", group.length);
        }
      });

      socket.on("addCardToGroup", (resp) => {
        const room = rooms[resp.roomId];
        const group = resp.group;
        const cardId = resp.cardId;
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canAddCardToGroup(room, socket.id, group, cardId)) {
          const cardIndex = player.hand.indexOf(cardId);
          const groupDefaultLength = group.length;
          player.hand.splice(cardIndex, 1);
          socket.emit("canAddCardToGroup");
          if (player.open) {
            if (isGroupOwnedByPlayer(player, group)) {
              player.table
                .find(
                  (tableGroup) =>
                    orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
                )
                .push(cardId);
            } else {
              const opponent = rules.getPlayerById(room, otherPlayerId);
              opponent.table
                .find(
                  (tableGroup) =>
                    orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
                )
                .push(cardId);
            }
            group.push(cardId);
            otherPlayerSocket.emit("opponentAddCardToGroupOpen", {
              group,
              index: cardIndex,
            });
          } else {
            player.table
              .find(
                (tableGroup) =>
                  orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
              )
              .push(cardId);
            otherPlayerSocket.emit("opponentAddCardToGroupClosed", {
              index: cardIndex,
              groupLength: groupDefaultLength,
            });
          }
        }
      });

      socket.on("openCards", (roomId) => {
        const room = rooms[roomId];
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canPlayerOpen(room, socket.id)) {
          socket.emit("canOpenCards");
          player.open = true;
          otherPlayerSocket.emit("opponentOpensCards", player.table);
        }
      });

      socket.on("replaceJoker", (resp) => {
        const room = rooms[resp.roomId];
        const group = resp.group;
        const cards = resp.cards;
        const player = rules.getPlayerById(room, socket.id);
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        if (rules.canReplaceJoker(room, socket.id)) {
          if (player.open) {
            if (isGroupOwnedByPlayer(player, group)) {
              const tableGroup = player.table.find(
                (tableGroup) =>
                  orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
              );

              replaceJokerInGroup(tableGroup, cards);
            } else {
              const opponent = rules.getPlayerById(room, otherPlayerId);
              const tableGroup = opponent.table.find(
                (tableGroup) =>
                  orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
              );
              replaceJokerInGroup(tableGroup, cards);
            }

            const cardIndexes = [];
            cards.forEach((card) => {
              cardIndexes.push(player.hand.indexOf(card));
            });

            otherPlayerSocket.emit("opponentReplaceJoker", {
              group,
              cardIndexes,
              cards,
            });
          } else {
            const tableGroup = player.table.find(
              (tableGroup) =>
                orgranizeGroup(tableGroup)[0] === orgranizeGroup(group)[0]
            );
            replaceJokerInGroup(tableGroup, cards);
          }
          socket.emit("canReplaceJoker");
        }
      });

      socket.on("exitRoom", (roomId) => {
        const room = rooms[roomId];
        const otherPlayerId = getOtherPlayerId(room, socket.id);
        const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
        otherPlayerSocket.emit("opponentLeft");
        socket.leave(roomId);
        otherPlayerSocket.leave(roomId);
      });

      socket.on("returnToLobby", (roomId) => {
        const room = rooms[roomId];
        socket.leave(roomId, () => {});
        socket.emit("leaveRoom");
        if (room) {
          const otherPlayerId = getOtherPlayerId(room, socket.id);
          const otherPlayerSocket = io.of("/").sockets.get(otherPlayerId);
          otherPlayerSocket.leave(roomId);
        }
      });
    });
  },
};

function getOtherPlayerId(room, playerId) {
  let otherPlayerId = "";
  if (room.player1.uuid === playerId) {
    otherPlayerId = room.player2.uuid;
  } else {
    otherPlayerId = room.player1.uuid;
  }

  return otherPlayerId;
}

function isGroupOwnedByPlayer(player, group) {
  let groupFound = false;
  player.table.forEach((tableGroup) => {
    if (group[0] === tableGroup[0]) {
      groupFound = true;
    }
  });
  return groupFound;
}

function getRoomByPlayerId(playerId, rooms) {
  for (let [key, value] of Object.entries(rooms)) {
    if (value.player1.uuid === playerId || value.player2.uuid === playerId) {
      return key;
    }
  }
}

function replaceJokerInGroup(tableGroup, cards) {
  const jokerId = tableGroup.find((card) => card >= 52);
  const jokerIndex = tableGroup.indexOf(jokerId);
  tableGroup.splice(jokerIndex, 1);
  cards.forEach((card) => {
    tableGroup.splice(jokerIndex, 0, card);
  });
}

function orgranizeGroup(group) {
  return group.sort((a, b) => parseInt(a, 10) - parseInt(b, 10));
}
