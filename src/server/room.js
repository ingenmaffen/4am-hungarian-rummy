module.exports = {
  initRoom: (uuid, player1Id, player2Id) => {
    const player1 = {
      uuid: player1Id,
      hand: [],
      table: [],
      open: false,
    };
    const player2 = {
      uuid: player2Id,
      hand: [],
      table: [],
      open: false,
    };
    let deck = [];

    // generate cards to deck
    let validDeck = false;
    while (!validDeck) {
      deck = [];
      for (let i = 0; i < 55; i++) {
        while (deck.length == i) {
          let rand = Math.floor(Math.random() * 55);
          let hasSame = 0;
          for (let j = 0; j < deck.length; j++) {
            if (deck[j] == rand) hasSame++;
          }
          if (hasSame == 0) deck.push(rand);
        }
      }

      // check if any player has 3 joker cards
      player1.hand = deck.splice(0, 15);
      player2.hand = deck.splice(0, 14);
      if (
        player1.hand.filter((cardId) => cardId >= 52).length < 3 &&
        player2.hand.filter((cardId) => cardId >= 52).length < 3
      ) {
        validDeck = true;
      }
    }
    return {
      uuid,
      deck,
      thrown: [],
      player1,
      player2,
    };
  },
};
